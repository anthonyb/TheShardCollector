package com.anthony.theshardcollector.entity.projectile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.anthony.foobar.game.entity.Entity;
import com.anthony.foobar.game.entity.Mob;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.enemy.Monster;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;

public class Projectile extends Mob {

	private int damage, life, speed, range;
	
	private boolean stickOnCollide, sentByEnemy;
	
	private Direction dir;
	
	protected Entity sender;
	
	public Projectile(int x, int y, int damage, int speed, int range, boolean stickOnCollide, Direction dir, String identifier, Sprite sprite, Level level, Entity sender) {
		
		super(x, y, 1, 999, level, new Rectangle(0, 0, sprite.getWidth(), sprite.getHeight()), identifier, sprite);
		
		this.damage = damage;
		this.life = 0;
		this.speed = speed;
		this.range = range;
		super.scale = 2;
		super.addToXRect = 12;
		super.addToYRect = 12;
		super.addToWidthRect = -25;
		super.addToHeightRect = -25;
		
		super.updateCollisionRect();
		
		this.stickOnCollide = stickOnCollide;
		this.sentByEnemy = sender instanceof Monster;
		
		this.dir = dir;
		
		this.sender = sender;
	}
	
	@Override
	public void tick() {
		
		this.life++;
		
		if (this.life >= this.range) {
			
			super.level.removeEntity(this);
			return;
		}
		
		if (this.sentByEnemy) {
			
			Player player = super.level.getPlayer();
			
			if (super.collisionRect.intersects(player.getCollisionRect())) {
				
//				FooBar.getLogger().info(super.level.getPlayer().getIdentifier() + " was hit!");
				
				boolean remove = player.hit(this);
				
				if (remove) {
					
					super.level.removeEntity(this);
					return;
				}
			}
		} else {
			
			for (Entity e : super.level.getEntities()) {
				
				if (e instanceof Mob) {
					
					if (e.equals(sender) || e.equals(this)) {
						
						continue;
					}
						
					Mob mob = (Mob) e;
					
					if (super.collisionRect.intersects(mob.getCollisionRect())) {
						
//						FooBar.getLogger().info(mob.getIdentifier() + " was hit!");
						
						boolean remove = mob.hit(this);
						
						if (remove) {
							
							super.level.removeEntity(this);
							return;
						}
					}
				}
			}
		}
		
		int moveX = this.speed * this.dir.getXDir(), moveY = this.speed * this.dir.getYDir();
		
		if (!super.isColliding(moveX, moveY)) {
			
			super.move(moveX, moveY);
		} else if (!this.stickOnCollide) {
			
			super.level.removeEntity(this);
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		super.render(screen);
	}
	
	@Override
	public void postRender(Graphics g) {
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		Rectangle rect = super.getCollisionRect();
		Camera camera = super.level.getCamera();
		
		g.setColor(Color.PINK);
		
		g.drawRect((int) (rect.getX() - camera.getX()), (int) (rect.getY() - camera.getY()), (int) rect.getWidth(), (int) rect.getHeight());
	}
	
	@Override
	public boolean hit(Projectile proj) {
		
		this.kill();
		
		return true;
	}
	
	@Override
	public void kill() {
		
		this.remove();
	}
	
	public int getDamage() {
		
		return this.damage;
	}
	
	public Entity getSender() {
		
		return this.sender;
	}
}