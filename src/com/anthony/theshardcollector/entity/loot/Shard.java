package com.anthony.theshardcollector.entity.loot;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.game.utils.UpDownInt;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;

public class Shard extends Loot {
	
	private static final VarianceSound s = new VarianceSound("/sounds/shard.wav", "/sounds/shard.wav", "/sounds/shard2.wav", "/sounds/shard3.wav");
	
	private int originalY;
	
	private UpDownInt yAxis;
	
	public Shard(int x, int y, Level level) {
		
		super(x, y, level, new Rectangle(0, 0, Sprite.SHARD.getWidth(), Sprite.SHARD.getHeight()));
		
		super.updateCollisionRect();
		
		this.originalY = y;
		
		this.yAxis = new UpDownInt(10, -5, 5);
	}
	
	@Override
	public void tick() {
		
		this.yAxis.tick();
		
		super.y = originalY + this.yAxis.get();
		super.updateCollisionRect();
		
		if (super.level.getPlayer().getCollisionRect().intersects(super.getCollisionRect())) {
			
			this.remove();
			Shard.s.getVarianceSound().play(.3D, false);
			return;
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.renderSprite(x, y, Sprite.SHARD);
	}
	
	@Override
	public void postRender(Graphics g) {
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		Rectangle rect = super.getCollisionRect();
		Camera camera = super.level.getCamera();
		
		g.setColor(Color.YELLOW);
		
		g.drawRect((int) (rect.getX() - camera.getX()), (int) (rect.getY() - camera.getY()), (int) rect.getWidth(), (int) rect.getHeight());
	}
	
	@Override
	public void remove() {
		
		super.level.getPlayer().addShard();
		super.remove();
	}
}