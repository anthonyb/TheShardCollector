package com.anthony.theshardcollector.entity.loot;

import java.awt.Rectangle;

import com.anthony.foobar.game.entity.Entity;
import com.anthony.theshardcollector.level.Level;

public abstract class Loot extends Entity {
	
	protected Rectangle collisionRect;
	
	public Loot(int x, int y, Level level, Rectangle collisionRect) {
		
		super(x, y, level);
		
		this.collisionRect = collisionRect;
	}
	
	@Override
	public void remove() {
		
		super.level.removeEntity(this);
	}
	
	public void updateCollisionRect() {
		
		this.collisionRect.setLocation(super.x, super.y);
	}
	
	public Rectangle getCollisionRect() {
		
		return this.collisionRect;
	}
}
