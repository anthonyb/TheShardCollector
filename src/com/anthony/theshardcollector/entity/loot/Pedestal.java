package com.anthony.theshardcollector.entity.loot;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.anthony.foobar.FooBar;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.AnimatedSprite;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.particle.ParticleSpawner;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.entity.player.mutations.Mutation;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;
import com.anthony.theshardcollector.level.tile.Tile;

public class Pedestal extends Loot {
	
	private static VarianceSound s = new VarianceSound("/sounds/mutate.wav", "/sounds/mutate.wav", "/sounds/mutate_alt.wav");
	
	private int scale, usedTimer;
	
	private boolean used;
	
	private AnimatedSprite animSprite;
	
	public Pedestal(int x, int y, Level level) {
		
		super(x * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, y * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, level, new Rectangle(0, 0, 0, 0));
		
		this.scale = 2 * Tile.TILE_SCALE;
		this.usedTimer = -1;
		
		this.used = false;
		
		this.animSprite = new AnimatedSprite(30, true, Sprite.PEDESTAL_1, Sprite.PEDESTAL_2, Sprite.PEDESTAL_3);
		
		super.collisionRect.setBounds(super.x, super.y + (this.scale * Sprite.PEDESTAL_1.getHeight() / 2), this.scale * Sprite.PEDESTAL_1.getWidth(), this.scale * Sprite.PEDESTAL_1.getHeight() / 2);
	}
	
	@Override
	public void tick() {
		
		this.animSprite.tick();
		
		if (this.usedTimer > 0) {
			
			this.usedTimer--;
		} else if (this.usedTimer == 0) {
			
			this.animSprite = new AnimatedSprite(30, true, Sprite.PEDESTAL_RED_1, Sprite.PEDESTAL_RED_2, Sprite.PEDESTAL_RED_3);
			this.usedTimer = -1;
		}
		
		if (this.used) {
			
			return;
		}
		
		Player player = super.level.getPlayer();
		
		boolean intersecting = player.getCollisionRect().intersects(this.collisionRect);
		
		if (intersecting) {
			
			Mutation nextMutation = Mutation.getNextMutation(player.getMutation());
			
			if (nextMutation == null) {
				
				return;
			}
			
			int cost = nextMutation.getShardCost();
			
			if (player.getShards() >= cost) {
				
				player.removeShards(cost);
				player.setMutation(nextMutation);
				
				super.level.addEntity(new ParticleSpawner(super.x - 7, super.y + 15, this.scale * Sprite.PEDESTAL_1.getWidth(), 10, 125 * 3, 5, Sprite.MUTATE_LINE_PARTICLE, super.level, Direction.NORTH));
				super.level.addEntity(new ParticleSpawner(super.x - 7, super.y + 15, this.scale * Sprite.PEDESTAL_1.getWidth(), 10, 125 * 2, 25, Sprite.MUTATE_DUST_PARTICLE, super.level, Direction.NORTH, Direction.NORTH_EAST, Direction.NORTH_WEST));
				
				Pedestal.s.getVarianceSound().play(.1, false);
				
				FooBar.getLogger().info("Mutated to " + nextMutation.name() + "! (-" + cost + " mutation shards)");
				
				this.used = true;
				
				this.usedTimer = 3 * 125;
				return;
			}
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.renderBrightnessSprite(50, false, x, y, scale, false, this.animSprite.getSprite());
	}
	
	@Override
	public void postRender(Graphics g) {
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		Rectangle rect = super.getCollisionRect();
		Camera camera = super.level.getCamera();
		
		g.setColor(Color.YELLOW);
		
		g.drawRect((int) (rect.getX() - camera.getX()), (int) (rect.getY() - camera.getY()), (int) rect.getWidth(), (int) rect.getHeight());
	}
}