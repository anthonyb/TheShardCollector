package com.anthony.theshardcollector.entity.particle;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.theshardcollector.direction.Direction;

public class Particle {
	
	private int x, y, goToX, goToY;
	
	private boolean xFinished, yFinished;
	
	private Sprite sprite;
	
	private ParticleSpawner ps;
	
	public Particle(int x, int y, int particleLife, Sprite sprite, Direction dir, ParticleSpawner ps) {
		
		this.x = x;
		this.y = y;
		
		this.goToX = this.x + dir.getXDir() * particleLife;
		this.goToY = this.y + dir.getYDir() * particleLife;
		
		this.xFinished = false;
		this.yFinished = false;
		
		this.sprite = sprite;
		
		this.ps = ps;
	}
	
	public void tick() {
		
		if (this.y > this.goToY) {
			
			this.y--;
		} else if (this.y < this.goToY) {
			
			this.y++;
		} else if (this.y == this.goToY) {
			
			this.yFinished = true;
		}
		
		if (this.x > this.goToX) {
			
			this.x--;
		} else if (this.x < this.goToX) {
			
			this.x++;
		} else {
			
			this.xFinished = true;
		}
		
		if (this.xFinished && this.yFinished) {
			
			this.ps.removeParticle(this);
		}
	}
	
	public void render(Screen screen) {
		
		screen.renderBrightnessSprite(50, false, this.x, this.y, 1, false, this.sprite);
	}
}
