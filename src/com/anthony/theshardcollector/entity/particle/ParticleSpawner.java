package com.anthony.theshardcollector.entity.particle;

import java.awt.Graphics;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import com.anthony.foobar.game.entity.Entity;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.level.Level;

public class ParticleSpawner extends Entity {
	
	private int width, height, life, particleLife, chanceOfNewSpawn, time;
	
	private Sprite sprite;
	
	private Direction[] directions;
	
	private CopyOnWriteArrayList<Particle> particles;
	
	private Random random;
	
	public ParticleSpawner(int x, int y, int width, int height, int life, int chanceOfNewSpawn, Sprite sprite, Level level, Direction... directions) {
		
		this(x, y, width, height, life, 50, chanceOfNewSpawn, sprite, level, directions);
	}
	
	public ParticleSpawner(int x, int y, int width, int height, int life, int particleLife, int chanceOfNewSpawn, Sprite sprite, Level level, Direction... directions) {
		
		super(x, y, level);
		
		this.width = width;
		this.height = height;
		this.life = life;
		this.particleLife = particleLife;
		this.chanceOfNewSpawn = chanceOfNewSpawn;
		this.time = 0;
		
		this.sprite = sprite;
		
		this.directions = directions;
		
		this.particles = new CopyOnWriteArrayList<Particle>();
		
		this.random = new Random();
	}
	
	@Override
	public void tick() {
		
		this.time++;
		
		for (Particle p : this.particles) {
			
			p.tick();
		}
		
		if (this.time >= this.life && this.life != -1) {
			
			if (this.particles.size() == 0) {
				
				super.level.removeEntity(this);
			}
			
			return;
		}
		
		if (this.random.nextInt(this.chanceOfNewSpawn) == 0) {
			
			this.particles.add(new Particle(super.x + this.random.nextInt(this.width), super.y + this.random.nextInt(this.height), this.particleLife, this.sprite, this.directions[this.random.nextInt(this.directions.length)], this));
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		for (Particle p : this.particles) {
			
			p.render(screen);
		}
	}
	
	@Override
	public void postRender(Graphics g) {}
	
	@Override
	public void remove() {
		
	}
	
	public void removeParticle(Particle p) {
		
		this.particles.remove(p);
	}
}