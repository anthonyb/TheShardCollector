package com.anthony.theshardcollector.entity.item;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.anthony.foobar.game.entity.Mob;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.game.utils.UpDownInt;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.particle.ParticleSpawner;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.entity.projectile.Projectile;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;
import com.anthony.theshardcollector.level.tile.Tile;

public class HealthPotion extends Mob {
	
	private static VarianceSound s = new VarianceSound("/sounds/potion.wav", "/sounds/potion.wav", "/sounds/potion_alt.wav");
	
	private UpDownInt upDownY;
	
	public HealthPotion(int x, int y, Level level) {
		
		super(x, y, 1, 999, level, new Rectangle(0, 0, Sprite.HEALTH_POTION.getWidth(), Sprite.HEALTH_POTION.getHeight()), "Health Potion", Sprite.HEALTH_POTION);
		
		super.scale = Tile.TILE_SCALE;
		super.addToXRect = 7;
		super.addToYRect = 3;
		super.addToWidthRect = -15;
		super.addToHeightRect = -8;
		
		super.updateCollisionRect();
		
		this.upDownY = new UpDownInt(5, -5, 5);
	}
	
	@Override
	public void tick() {
		
		this.upDownY.tick();
		
		Player player = super.level.getPlayer();
		
		if (super.collisionRect.intersects(player.getCollisionRect())) {
			
			player.addHealth(20);
			
			HealthPotion.s.getVarianceSound().play(.3, false);
			
			super.level.addEntity(new ParticleSpawner(super.x, super.y, Sprite.HEALTH_POTION.getWidth(), Sprite.HEALTH_POTION.getHeight(), 125 / 2, 50, 20, Sprite.DUST_RED_PARTICLE, super.level, Direction.NORTH));
			super.level.removeEntity(this);
			return;
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.renderSprite(this.x, this.y + this.upDownY.get(), this.scale, false, this.sprite);
	}
	
	@Override
	public void postRender(Graphics g) {
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		Rectangle rect = super.getCollisionRect();
		Camera camera = super.level.getCamera();
		
		g.setColor(Color.GREEN);
		
		g.drawRect((int) (rect.getX() - camera.getX()), (int) (rect.getY() - camera.getY()), (int) rect.getWidth(), (int) rect.getHeight());
	}
	
	@Override
	public boolean hit(Projectile proj) {
		
		return false;
	}
}
