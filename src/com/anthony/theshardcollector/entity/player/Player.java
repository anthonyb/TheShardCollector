package com.anthony.theshardcollector.entity.player;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.anthony.foobar.game.entity.Entity;
import com.anthony.foobar.game.entity.Mob;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.AnimatedSprite;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.sound.Sound;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.Main;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.door.Door;
import com.anthony.theshardcollector.entity.enemy.Monster;
import com.anthony.theshardcollector.entity.enemy.MonsterB;
import com.anthony.theshardcollector.entity.item.HealthPotion;
import com.anthony.theshardcollector.entity.loot.Pedestal;
import com.anthony.theshardcollector.entity.particle.ParticleSpawner;
import com.anthony.theshardcollector.entity.player.mutations.Mutation;
import com.anthony.theshardcollector.entity.projectile.Projectile;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;
import com.anthony.theshardcollector.level.menu.DeathMenu;
import com.anthony.theshardcollector.level.menu.MainMenu;
import com.anthony.theshardcollector.level.menu.WinMenu;
import com.anthony.theshardcollector.level.tile.Tile;
import com.anthony.theshardcollector.utils.MathHelper;

public class Player extends Mob {
	
	private static VarianceSound
		player_hurt_sound = new VarianceSound("/sounds/player_hurt.wav"),
		player_m0_shoot = new VarianceSound("/sounds/shoot.wav"),
		player_m1_shoot = new VarianceSound("/sounds/shoot_m1.wav"),
		player_m2_shoot = new VarianceSound("/sounds/shoot_m2.wav")
	;
	
	private int speed, shards, projectileTimer, levelNum;
	
	private boolean canMutate;
	
	private Mutation mutation;
	
	private AnimatedSprite animSprite;
	
	private Camera camera;
	
	public Player(Level level) {
		
		super(3 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 9 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 1, 100, level, new Rectangle(0, 0, Sprite.M0_FORWARD_1.getWidth(), Sprite.M0_FORWARD_1.getHeight()), "Player", Sprite.M0_FORWARD_1);
		
		super.scale = 2;
		
		this.shards = this.projectileTimer = 0;
		this.levelNum = 1;
		
		this.canMutate = false;
		
		this.setMutation(Mutation.M0);
		
		this.animSprite = this.mutation.getSprites()[2];
		
		this.camera = level.getCamera();
	}
	
	@Override
	public void tick() {
		
		super.sprite = this.animSprite.getSprite();
		
		if (this.mutation.getOnlyAnimateMoving()) {
			
			if (Key.W.getPressed() || Key.A.getPressed() || Key.S.getPressed() || Key.D.getPressed()) {
				
				this.animSprite.tick();
			} else {
				
				this.animSprite.setFrame(0);
			}
		} else {
			
			this.animSprite.tick();
		}
		
//		TODO condense the below code!  BUT AT LEAST IT WORKS!
		int
			moveX = 0,
			moveY = 0,
			lastYPixel = this.level.getHeight() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE,
			lastXPixel = this.level.getWidth() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE
		;
		
		AnimatedSprite[] sprites = this.mutation.getSprites();
		
		if (Key.W.getPressed()) {
			
			this.animSprite = sprites[3];
			super.sprite = this.animSprite.getSprite();
			
			if (super.y + super.addToYRect > 0 && (super.y + super.addToYRect) + super.scale * (super.sprite.getHeight() + super.addToHeightRect) < lastYPixel) {
				
				if (super.y + super.addToYRect - this.speed > 0 && super.y + super.addToYRect - this.speed + super.scale * (super.sprite.getHeight() + super.addToHeightRect) < lastYPixel) {
					
					moveY -= this.speed;
				} else {
					
					moveY -= super.y + super.addToYRect - 1;
				}
			}
		}
		
		if (Key.S.getPressed()) {
			
			this.animSprite = sprites[2];
			super.sprite = this.animSprite.getSprite();
			
			if (super.y + super.addToYRect > 0 && (super.y + super.addToYRect) + super.scale * (super.sprite.getHeight() + super.addToHeightRect) < lastYPixel) {
				
				if (super.y + super.addToYRect + this.speed > 0 && super.y + super.addToYRect + this.speed + super.scale * (super.sprite.getHeight() + super.addToHeightRect) < lastYPixel) {
					
					moveY += this.speed;
				} else {
					
					moveY += lastYPixel - ((super.y + super.addToYRect) + super.scale * (super.sprite.getHeight() + super.addToHeightRect)) - 1;
				}
			}
		}
		
		if (Key.A.getPressed()) {
			
			this.animSprite = sprites[1];
			super.sprite = this.animSprite.getSprite();
			
			if (super.x + super.addToXRect > 0 && (super.x + super.addToXRect) + super.scale * (super.sprite.getWidth() + super.addToWidthRect) < lastXPixel) {
				
				if (super.x + super.addToXRect - this.speed > 0 && super.x + super.addToXRect - this.speed + super.scale * (super.sprite.getWidth() + super.addToWidthRect) < lastXPixel) {
					
					moveX -= this.speed;
				} else {
					
					moveX -= super.x + super.addToXRect - 1;
				}
			}
		}
		
		if (Key.D.getPressed()) {
			
			this.animSprite = sprites[0];
			super.sprite = this.animSprite.getSprite();
			
			if (super.x + super.addToXRect > 0 && (super.x + super.addToXRect) + super.scale * (super.sprite.getWidth() + super.addToWidthRect) < lastXPixel) {
				
				if (super.x + super.addToXRect + this.speed > 0 && super.x + super.addToXRect + this.speed + super.scale * (super.sprite.getWidth() + super.addToWidthRect) < lastXPixel) {
					
					moveX += this.speed;
				} else {
					
					moveX += lastXPixel - (super.x + super.addToXRect + super.scale * (super.sprite.getWidth() + super.addToWidthRect)) - 1;
				}
			}
		}
		
		if (this.projectileTimer == 0) {
			
			this.checkProjectile(moveX, moveY);
		} else {
			
			this.projectileTimer--;
		}
		
		if (!super.isColliding(moveX, moveY)) {
			
			super.move(moveX, moveY);
			
			if (super.y < super.level.getHeight() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE - this.camera.getHalfScreenHeight() && Key.W.getPressed() && this.camera.getY() > 0) {
				
				this.camera.setY(this.camera.getY() - this.speed);
			}
			if (super.y > this.camera.getHalfScreenHeight() && Key.S.getPressed() && this.camera.getY() + this.speed < super.level.getHeight() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE - this.camera.getScreenHeight()) {
				
				this.camera.setY(this.camera.getY() + this.speed);
			}
			
			if (super.x < super.level.getWidth() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE - this.camera.getHalfScreenWidth() && Key.A.getPressed() && this.camera.getX() > 0) {
				
				this.camera.setX(this.camera.getX() - this.speed);
			}
			if (super.x > this.camera.getHalfScreenWidth() && Key.D.getPressed() && this.camera.getX() + this.speed < super.level.getWidth() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE - this.camera.getScreenWidth()) {
				
				this.camera.setX(this.camera.getX() + this.speed);
			}
		}
		
		super.tick();
	}
	
	@Override
	public void render(Screen screen) {
		
		super.render(screen);
		
		screen.renderPortionSprite(0, super.health, 0, 10, 10, 10, true, Sprite.HEALTH_BAR);
		screen.renderSprite(10, 25, 1, true, Sprite.SHARD);
	}
	
	@Override
	public void postRender(Graphics g) {
		
		g.setColor(Color.BLACK);
		g.fillRect(32, 25, 10 * MathHelper.getDigits(this.shards), 15);
		
		Rectangle rect = super.getCollisionRect();
		
		g.setColor(this.canMutate ? Color.YELLOW : Color.WHITE);
		
		g.setFont(new Font("Verdana", 0, 15));
		
		g.drawString(this.shards + "", 32, 38);
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		g.setColor(Color.CYAN);
		
		g.drawRect((int) (rect.getX() - this.camera.getX()), (int) (rect.getY() - this.camera.getY()), (int) rect.getWidth(), (int) rect.getHeight());
	}
	
	@Override
	public void kill() {
		
		super.kill();
		this.camera.setX(0);
		this.camera.setY(0);
		Main.instance.setLevel(new DeathMenu());
	}
	
	@Override
	public boolean hit(Projectile proj) {
		
		Player.player_hurt_sound.getPrimarySound().play(1, false);
		this.subtractHealth(proj.getDamage());
		return true;
	}
	
	@Override
	public void subtractHealth(int damage) {
		
		if (super.damageTimer == 0) {
			
			super.level.addEntity(new ParticleSpawner(super.x, super.y, super.sprite.getWidth(), super.sprite.getHeight(), 50, 15, Sprite.BLOOD_DUST_PARTICLE, super.level, Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST));
		}
		
		super.subtractHealth(damage);
	}
	
	public void checkProjectile(int moveX, int moveY) {
		
		boolean shootProjectile = false;
		
		Direction dir = Direction.NORTH;
		
		if (Key.UP.getPressed()) {
			
			dir = Direction.NORTH;
			
			if (this.speed > 1) {
			
				if (moveX > 0) {
					
					dir = Direction.NORTH_EAST;
				} else if (moveX < 0) {
					
					dir = Direction.NORTH_WEST;
				}
			}
			
			shootProjectile = true;
		} else if (Key.LEFT.getPressed()) {
			
			dir = Direction.WEST;
			
			if (this.speed > 1) {
				
				if (moveY > 0) {
					
					dir = Direction.SOUTH_WEST;
				} else if (moveY < 0) {
					
					dir = Direction.NORTH_WEST;
				}
			}
			
			shootProjectile = true;
		} else if (Key.DOWN.getPressed()) {
			
			dir = Direction.SOUTH;
			
			if (this.speed > 1) {
				
				if (moveX > 0) {
					
					dir = Direction.SOUTH_EAST;
				} else if (moveX < 0) {
					
					dir = Direction.SOUTH_WEST;
				}
			}
			
			shootProjectile = true;
		} else if (Key.RIGHT.getPressed()) {
			
			dir = Direction.EAST;
			
			if (this.speed > 1) {
				
				if (moveY > 0) {
					
					dir = Direction.SOUTH_EAST;
				} else if (moveY < 0) {
					
					dir = Direction.NORTH_EAST;
				}
			}
			
			shootProjectile = true;
		}
		
		if (shootProjectile) {
			
			int damage, speed = 0, range;
			boolean stickOnCollide;
			Sprite projSprite;
			String identifier;
			
			if (moveX != 0 || moveY != 0) {
				
				Direction playerDir = Direction.getDirection(moveX, moveY);
				
				if (dir.equals(playerDir)) {
					
					speed += this.speed;
				}
				
				if (dir.equals(Direction.opposite(playerDir))) {
					
					speed -= this.speed;
					
					if (this.mutation.equals(Mutation.M2)) {
						
						this.projectileTimer = 0;
						return;
					}
				}
			}
			
			switch (this.mutation) {
			case M0:
				
				damage = 10;
				speed += 3;
				range = 125;
				stickOnCollide = true;
				projSprite = Sprite.SLIME_BALL_PROJECTILE;
				identifier = "Slime Ball Projectile";
				
				this.projectileTimer = 50;
				
				player_m0_shoot.getPrimarySound().play(.15, false);
				break;
			case M1:
				
				damage = 9;
				speed += 5;
				range = 50;
				stickOnCollide = false;
				projSprite = Sprite.TEAR_BALL_PROJECTILE;
				identifier = "Tear Ball Projectile";
				
				this.projectileTimer = 45;
				
				player_m1_shoot.getPrimarySound().play(.15, false);
				break;
			case M2:
				
				damage = 7;
				speed += 3;
				range = 100;
				stickOnCollide = true;
				projSprite = Sprite.POOP_BALL_PROJECTILE;
				identifier = "Poop Ball Projectile";
				
				this.projectileTimer = 20;
				
				player_m2_shoot.getPrimarySound().play(.15, false);
				break;
			default:
				
				damage = 10;
				speed += 3;
				range = 125;
				stickOnCollide = true;
				projSprite = Sprite.SLIME_BALL_PROJECTILE;
				identifier = "Slime Ball Projectile";
				
				player_m0_shoot.getPrimarySound().play(.15, false);
				break;
			}
			
			super.level.addEntity(new Projectile(super.x + (super.sprite.getWidth() / 2), super.y + (super.sprite.getHeight() / 2), damage, speed, range, stickOnCollide, dir, identifier, projSprite, super.level, this));
		}
	}
	
	public void addShard() {
		
		this.shards++;
		
		Mutation nextMutation = Mutation.getNextMutation(this.mutation);
		
		if (nextMutation != null && this.shards >= nextMutation.getShardCost()) {
			
			this.canMutate = true;
		}
	}
	
	public void removeShards(int shards) {
		
		this.shards -= shards;
	}
	
	public void setMutation(Mutation mutation) {
		
		this.mutation = mutation;
		
		switch (this.mutation) {
		case M0:
			
			this.speed = 1;
			
			super.addToXRect = 5;
			super.addToYRect = 5;
			super.addToWidthRect = -12;
			super.addToHeightRect = -12;
			break;
		case M1:
			
			this.speed = 3;
			
			super.addToXRect = 17;
			super.addToYRect = 17;
			super.addToWidthRect = -35;
			super.addToHeightRect = -35;
			
			for (Tile tile : super.level.getTiles()) {
				
				tile.setShapeShift(true);
			}
			
			Main.setMainLoop(new Sound("/sounds/heartbeat.wav"), 1);
			
			switch (this.levelNum) {
			case 1:
				
				super.level.addEntity(new Pedestal(45, 3, super.level));
				
				super.level.addEntity(new Monster(4 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 3 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(14 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 13 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(18 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 22 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(23 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(32 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 13 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(40 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 22 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(47 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 10 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				
				break;
			case 2:
				
				super.level.addEntity(new Pedestal(6, 14, super.level));
				
				for (Entity e : super.level.getEntities()) {
					
					if (e instanceof HealthPotion) {
						
						HealthPotion hp = (HealthPotion) e;
						
						if (hp.getX() == 7 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE && hp.getY() == 81 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE) {
							
							super.level.removeEntity(hp);
						}
					}
				}
				
				super.level.addEntity(new Monster(31 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 28 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(31 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 28 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(17 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(17 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(31 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 56 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 56 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(56 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 37 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(35 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 17 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(37 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 75 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(37 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 75 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(34 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 84 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(68 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 36 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				
				break;
			}
			break;
		case M2:
			
			this.speed = 2;
			
			super.addToXRect = 5;
			super.addToYRect = 10;
			super.addToWidthRect = -12;
			super.addToHeightRect = -13;
			
			for (Tile tile : super.level.getTiles()) {
				
				tile.setShapeShift(false);
			}
			
			Main.setMainLoop(new Sound("/sounds/music.wav"), .3);
			
			switch (this.levelNum) {
			case 1:
				
				super.level.addEntity(new MonsterB(47 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 23 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				
				break;
			case 2:
				
				super.level.addEntity(new Door(43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 87 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, this.levelNum, super.level));
				
				super.level.addEntity(new MonsterB(6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 39 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(19 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 68 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new Monster(55 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 95 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				super.level.addEntity(new MonsterB(54 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 83 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, super.level));
				
				break;
			}
			
			break;
		}
		
		this.canMutate = false;
	}
	
	public void setLevel(int level) {
		
		this.levelNum = level;
		
		Level nextLevel;
		
		switch (level) {
		case 2:
			
			this.setMutation(Mutation.M0);
			super.x = 7 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE;
			super.y = 7 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE;
			
			this.camera.setX(0);
			this.camera.setY(0);
			
			nextLevel = new Level("/levels/level2.png", this.camera);
			
			nextLevel.addEntity(new Pedestal(32, 43, nextLevel));
			
			nextLevel.addEntity(new HealthPotion(7 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 81 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new HealthPotion(22 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 84 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			
//			ADDED FOR EASY VERSION
//			nextLevel.addEntity(new HealthPotion(68 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
//			nextLevel.addEntity(new HealthPotion(6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 42 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
//			----------------------
			
			nextLevel.addEntity(new Monster(16 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new MonsterB(33 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new MonsterB(68 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 25 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new MonsterB(49 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 67 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new MonsterB(25 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 67 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new Monster(6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 34 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new Monster(17 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 20 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new Monster(56 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 18 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new MonsterB(43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 57 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new MonsterB(43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 31 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new Monster(26 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 29 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			nextLevel.addEntity(new Monster(45 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 39 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, nextLevel));
			
			nextLevel.setPlayer(this);
			
			Mutation.M1.setShardCost(10);
			Mutation.M2.setShardCost(15);
			
			Main.setMainLoop(new Sound("/sounds/music.wav"), .2);
			break;
		case 3:
			
//			For win particles to show.
			this.camera.setX(0);
			this.camera.setY(0);
			nextLevel = new WinMenu();
			break;
			default:
				
				nextLevel = new MainMenu(Main.instance.getHeight());
				break;
		}
		
		for (Entity e : super.level.getEntities()) {
			
			super.level.removeEntity(e);
		}
		
		super.level = nextLevel;
		Main.instance.setLevel(nextLevel);
	}
	
	public int getShards() {
		
		return this.shards;
	}
	
	public Mutation getMutation() {
		
		return this.mutation;
	}
}