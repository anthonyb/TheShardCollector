package com.anthony.theshardcollector.entity.player.mutations;

import com.anthony.foobar.game.graphics.sprite.AnimatedSprite;
import com.anthony.foobar.game.graphics.sprite.Sprite;

public enum Mutation {
	
	M0(0, false, new AnimatedSprite(75, false, Sprite.M0_RIGHT_1, Sprite.M0_RIGHT_2), new AnimatedSprite(75, false, Sprite.M0_LEFT_1, Sprite.M0_LEFT_2), new AnimatedSprite(75, false, Sprite.M0_FORWARD_1, Sprite.M0_FORWARD_2), new AnimatedSprite(75, false, Sprite.M0_BACK_1, Sprite.M0_BACK_2)),
	M1(4, false, new AnimatedSprite(20, false, Sprite.M1_RIGHT_1, Sprite.M1_RIGHT_2, Sprite.M1_RIGHT_3), new AnimatedSprite(20, false, Sprite.M1_LEFT_1, Sprite.M1_LEFT_2, Sprite.M1_LEFT_3), new AnimatedSprite(50, true, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_2, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_3), new AnimatedSprite(Sprite.M1_BACK)),
	M2(6, true, new AnimatedSprite(15, true, Sprite.M2_RIGHT_1, Sprite.M2_RIGHT_2, Sprite.M2_RIGHT_3), new AnimatedSprite(15, true, Sprite.M2_LEFT_1, Sprite.M2_LEFT_2, Sprite.M2_LEFT_3), new AnimatedSprite(15, true, Sprite.M2_RIGHT_1, Sprite.M2_RIGHT_2, Sprite.M2_RIGHT_3), new AnimatedSprite(15, true, Sprite.M2_LEFT_1, Sprite.M2_LEFT_2, Sprite.M2_LEFT_3))
	;
	
	private int shardCost;
	
	private boolean onlyAnimateMoving;
	
	private AnimatedSprite[] sprites;
	
	private Mutation(int shardCost, boolean onlyAnimateMoving, AnimatedSprite right, AnimatedSprite left, AnimatedSprite forward, AnimatedSprite backward) {
		
		this.shardCost = shardCost;
		
		this.onlyAnimateMoving = onlyAnimateMoving;
		
		this.sprites = new AnimatedSprite[4];
		
		this.sprites[0] = right;
		this.sprites[1] = left;
		this.sprites[2] = forward;
		this.sprites[3] = backward;
	}
	
	public void setShardCost(int cost) {
		
		this.shardCost = cost;
	}
	
	public int getShardCost() {
		
		return this.shardCost;
	}
	
	public boolean getOnlyAnimateMoving() {
		
		return this.onlyAnimateMoving;
	}
	
	public AnimatedSprite[] getSprites() {
		
		return this.sprites;
	}
	
	public static Mutation getNextMutation(Mutation m) {
		
		Mutation[] values = Mutation.values();
		
		for (int i = 0; i < values.length - 1; i++) {
			
			if (values[i] == m) {
				
				return values[i + 1];
			}
		}
		
		return null;
	}
}