package com.anthony.theshardcollector.entity.door;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.anthony.foobar.game.entity.Mob;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.entity.projectile.Projectile;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;

public class Door extends Mob {
	
	private int currentLevel;
	
	public Door(int x, int y, int currentLevel, Level level) {
		
		super(x, y, 1, 999, level, new Rectangle(0, 0, Sprite.DOOR.getWidth(), Sprite.DOOR.getHeight()), "Door", Sprite.DOOR);
		
		super.scale = 2;
		super.addToYRect = 5;
		super.updateCollisionRect();
		
		this.currentLevel = currentLevel;
	}
	
	@Override
	public void tick() {
		
		Player player = super.level.getPlayer();
		
		if (super.collisionRect.intersects(player.getCollisionRect())) {
			
			player.setLevel(this.currentLevel + 1);
			return;
		}
	}
	
	@Override
	public void postRender(Graphics g) {
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		Rectangle rect = super.getCollisionRect();
		Camera camera = super.level.getCamera();
		
		g.setColor(Color.ORANGE);
		
		g.drawRect((int) (rect.getX() - camera.getX()), (int) (rect.getY() - camera.getY()), (int) rect.getWidth(), (int) rect.getHeight());
	}
	
	@Override
	public boolean hit(Projectile proj) {
		
		return false;
	}
}
