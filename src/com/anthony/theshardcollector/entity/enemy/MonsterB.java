package com.anthony.theshardcollector.entity.enemy;

import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.level.Level;

public class MonsterB extends Monster {

	public MonsterB(int x, int y, Level level) {
		
		super(x, y, 10, level);
	}
	
	@Override
	public void changeMovement() {
		
		Player player = super.level.getPlayer();
		int xa = player.getX() - super.x, ya = player.getY() - super.y;
		
		if (Math.abs(xa) < this.scale * super.sprite.getWidth()) {
			
			xa = 0;
		}
		
		if (Math.abs(ya) < this.scale * super.sprite.getHeight()) {
			
			ya = 0;
		}
		
		super.setDirection(Direction.getDirection(xa, ya));
	}
}