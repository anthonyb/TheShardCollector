package com.anthony.theshardcollector.entity.enemy;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

import com.anthony.foobar.game.entity.Mob;
import com.anthony.foobar.game.graphics.sprite.AnimatedSprite;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.entity.projectile.Projectile;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;
import com.anthony.theshardcollector.level.tile.Tile;

public class Monster extends Mob {
	
	private static VarianceSound s = new VarianceSound("/sounds/enemy_hurt.wav");
	
	private int chanceOfProjectile, changeDirectionWait, speed, projectileWait;
	
	private AnimatedSprite animSprite, walkRight, walkLeft, walkForward, walkBack;
	
	private Random random;
	
	private Direction dir;
	
	public Monster(int x, int y, Level level) {
		
		this(x, y, 50, level);
	}
	
	public Monster(int x, int y, int chanceOfProjectile, Level level) {
		
		super(x, y, 2, 30, level, new Rectangle(0, 0, Sprite.MONSTER_FORWARD_1.getWidth(), Sprite.MONSTER_FORWARD_1.getHeight()), "Monster", Sprite.MONSTER_FORWARD_1);
		
		this.chanceOfProjectile = chanceOfProjectile;
		this.changeDirectionWait = 1 * 125;
		this.speed = 1;
		this.projectileWait = 0;
		
		super.scale = 2;
		super.addToXRect = 5;
		super.addToYRect = 10;
		super.addToWidthRect = -15;
		super.addToHeightRect = -23;
		
		super.updateCollisionRect();
		
		this.walkRight = new AnimatedSprite(
			
			10, true,
			Sprite.MONSTER_RIGHT_1,
			Sprite.MONSTER_RIGHT_2,
			Sprite.MONSTER_RIGHT_3
		);
		
		this.walkLeft = new AnimatedSprite(
				
			10, true,
			Sprite.MONSTER_LEFT_1,
			Sprite.MONSTER_LEFT_2,
			Sprite.MONSTER_LEFT_3
		);
		
		this.walkForward = new AnimatedSprite(
			
			10, true,
			Sprite.MONSTER_FORWARD_1,
			Sprite.MONSTER_FORWARD_2,
			Sprite.MONSTER_FORWARD_3
		);
		
		this.walkBack = new AnimatedSprite(
			
			10, true,
			Sprite.MONSTER_BACK_1,
			Sprite.MONSTER_BACK_2,
			Sprite.MONSTER_BACK_3
		);
		
		this.random = new Random();
		
		this.setDirection(Direction.values()[this.random.nextInt(Direction.values().length)]);
	}
	
	@Override
	public void tick() {
		
		this.animSprite.tick();
		super.sprite = this.animSprite.getSprite();
		
		Player player = super.level.getPlayer();
		
//		int playerX = player.getX(), playerY = player.getY();
		
		int moveX = 0, moveY = 0;
		
		if (this.changeDirectionWait == 0) {
		
			if (this.random.nextInt(2) == 0) {
				
				this.changeMovement();
			}
			
			this.changeDirectionWait = ((1 + this.random.nextInt(3)) * 125);
		} else {
			
			this.changeDirectionWait--;
		}
		
		moveX = this.dir.getXDir();
		moveY = this.dir.getYDir();
		
		if (super.collisionRect.intersects(player.getCollisionRect())) {
			
			player.subtractHealth(25);
		}
		
		moveX *= this.speed;
		moveY *= this.speed;
		
		if (this.projectileWait == 0 && this.random.nextInt(this.chanceOfProjectile) == 0) {
			
			int xa = player.getX() - super.x, ya = player.getY() - super.y;
			
			if (Math.abs(xa) < this.scale * super.sprite.getWidth()) {
				
				xa = 0;
			}
			
			if (Math.abs(ya) < this.scale * super.sprite.getHeight()) {
				
				ya = 0;
			}
			
			Direction playerDirection = Direction.getDirection(xa, ya);
			
			this.shootProjectile(moveX, moveY, playerDirection);
			
			this.projectileWait = 2 * 125;
		} else if (this.projectileWait > 0) {
			
			this.projectileWait--;
		}
		
		int newX = super.x + moveX, newY = super.y + moveY;
		
		if (!super.isColliding(moveX, moveY) && newX > 0 && newY > 0 && newX + super.scale * super.sprite.getWidth() < this.level.getWidth() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE && newY + super.scale * super.sprite.getHeight() < this.level.getHeight() * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE) {
		
			super.move(moveX, moveY);
		} else {
			
			this.setDirection(Direction.opposite(dir));
		}
		
		super.tick();
	}
	
	@Override
	public void postRender(Graphics g) {
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		Rectangle rect = super.getCollisionRect();
		Camera camera = super.level.getCamera();
		
		g.setColor(Color.YELLOW);
		
		g.drawRect((int) (rect.getX() - camera.getX()), (int) (rect.getY() - camera.getY()), (int) rect.getWidth(), (int) rect.getHeight());
	}
	
	@Override
	public boolean hit(Projectile proj) {
		
		if (proj.getSender().equals(super.level.getPlayer())) {
			
			Monster.s.getPrimarySound().play(.5, false);
			super.subtractHealth(proj.getDamage());
			return true;
		}
		
		return false;
	}
	
	public void shootProjectile(int moveX, int moveY, Direction dir) {
		
		int projectileSpeed = 3;
		
		if (moveX != 0 || moveY != 0) {
			
			Direction playerDir = Direction.getDirection(moveX, moveY);
			
			if (dir.equals(playerDir)) {
				
				projectileSpeed += this.speed;
			}
			
			if (dir.equals(Direction.opposite(playerDir))) {
				
				projectileSpeed -= this.speed;
			}
		}
		
		super.level.addEntity(new Projectile(super.x + (super.sprite.getWidth() / 2), super.y + (super.sprite.getHeight() / 2), 10, projectileSpeed, 125, false, dir, "Enemy Projectile", Sprite.ENEMY_PROJECTILE, super.level, this));
	}
	
	public void changeMovement() {
		
		this.setDirection(Direction.values()[this.random.nextInt(Direction.values().length)]);
	}
	
	public void setDirection(Direction dir) {
		
		this.dir = dir;
		
		if (this.dir.equals(Direction.NORTH)) {
			
			this.animSprite = this.walkBack;
		} else if (this.dir.equals(Direction.SOUTH)) {
			
			this.animSprite = this.walkForward;
		} else if (this.dir.name().endsWith("EAST")) {
			
			this.animSprite = this.walkRight;
		} else if (this.dir.name().endsWith("WEST")) {
			
			this.animSprite = this.walkLeft;
		}
	}
}