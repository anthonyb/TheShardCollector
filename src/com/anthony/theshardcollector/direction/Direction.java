package com.anthony.theshardcollector.direction;

public enum Direction {
	
	NORTH(0, -1),
	SOUTH(0, +1),
	EAST(+1, 0),
	WEST(-1, 0),
	NORTH_EAST(+1, -1),
	SOUTH_EAST(+1, +1),
	NORTH_WEST(-1, -1),
	SOUTH_WEST(-1, +1)
	;
	
	private int xDir, yDir;
	
	private Direction(int xDir, int yDir) {
		
		this.xDir = xDir;
		this.yDir = yDir;
	}
	
	public int getXDir() {
		
		return this.xDir;
	}
	
	public int getYDir() {
		
		return this.yDir;
	}
	
	public static Direction opposite(Direction dir) {
		
		switch (dir) {
		case NORTH:
			return Direction.SOUTH;
		case SOUTH:
			return Direction.NORTH;
		case EAST:
			return Direction.WEST;
		case WEST:
			return Direction.EAST;
		case NORTH_EAST:
			return Direction.SOUTH_WEST;
		case NORTH_WEST:
			return Direction.SOUTH_EAST;
		case SOUTH_EAST:
			return Direction.NORTH_WEST;
		case SOUTH_WEST:
			return Direction.NORTH_EAST;
			default:
				return Direction.NORTH;
		}
	}
	
	public static Direction getDirection(int dirX, int dirY) {
		
		if (dirX > 0) {
			
			dirX = +1;
		} else if (dirX < 0) {
			
			dirX = -1;
		} else {
			
			dirX = 0;
		}
		
		if (dirY > 0) {
			
			dirY = +1;
		} else if (dirY < 0) {
			
			dirY = -1;
		} else {
			
			dirY = 0;
		}
		
		for (Direction dir : Direction.values()) {
			
			if (dir.getXDir() == dirX && dir.getYDir() == dirY) {
				
				return dir;
			}
		}
		
		return Direction.NORTH;
	}
}