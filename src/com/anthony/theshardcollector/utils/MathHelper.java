package com.anthony.theshardcollector.utils;

public class MathHelper {
	
	public static int getDigits(int number) {
		
		if (number < 10) {
			
			return 1;
		}
		
		return 1 + MathHelper.getDigits(number / 10);
	}
}
