package com.anthony.theshardcollector;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.anthony.foobar.game.Game;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.sound.Sound;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.entity.door.Door;
import com.anthony.theshardcollector.entity.enemy.Monster;
import com.anthony.theshardcollector.entity.enemy.MonsterB;
import com.anthony.theshardcollector.entity.item.HealthPotion;
import com.anthony.theshardcollector.entity.loot.Pedestal;
import com.anthony.theshardcollector.entity.loot.Shard;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.entity.player.mutations.Mutation;
import com.anthony.theshardcollector.input.Keyboard;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;
import com.anthony.theshardcollector.level.menu.MainMenu;
import com.anthony.theshardcollector.level.tile.Tile;

public class ShapeshiftGame extends Game {
	
	private Level level;
	
	public ShapeshiftGame() {
		
		super(750, 450, 125, Main.GAME_NAME);
		
		super.addKeyListener(new Keyboard());
		
		new Thread("running") {
			
			@Override
			public void run() {
				
				startrun();
			}
		}.start();
		
		this.level = new MainMenu(super.getHeight());
	}
	
	@Override
	public void onTick() {
		
		if (this.level == null) {
			
			return;
		}
		
		this.level.tick();
	}
	
	@Override
	public void onRender(Screen screen) {
		
		if (this.level == null) {
			
			return;
		}
		
		this.level.render(screen);
	}

	@Override
	public void onPostRender(Graphics g) {
		
		if (this.level == null) {
			
			return;
		}
		
		this.level.postRender(g);
		
		if (!Key.X.getPressed()) {
			
			return;
		}
		
		g.setColor(Color.RED);
		g.setFont(new Font("Verdana", 1, 15));
		g.drawString("FPS " + super.getFPS() + " | TPS " + super.getTicks(), 200, 15);
	}
	
	public void setUpFirstLevelEntities() {
		
		super.screen.setBrightness(50);
		
		Mutation.M1.setShardCost(4);
		Mutation.M2.setShardCost(6);
		
		this.level.addEntity(new Shard(4 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 6 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, this.level));
		
		this.level.addEntity(new Pedestal(4, 22, this.level));
		
		this.level.addEntity(new Monster(23 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 7 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, this.level));
		this.level.addEntity(new Monster(43 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 13 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, this.level));
		this.level.addEntity(new MonsterB(22 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 20 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, this.level));
		this.level.addEntity(new MonsterB(5 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 20 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, this.level));
		
		this.level.addEntity(new HealthPotion((int) (21.5 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE), 2 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, this.level));
		
		this.level.addEntity(new Door(47 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 23 * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE, 1, this.level));
		
		this.level.setPlayer(new Player(this.level));
	}
	
	public void setLevel(Level level) {
		
		Main.setMainLoop(new Sound("/sounds/music.wav"), .7);
		this.level = level;
	}
	
	public Camera getCamera() {
		
		return this.camera;
	}
}