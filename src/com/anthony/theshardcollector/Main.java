package com.anthony.theshardcollector;

import java.io.File;

import com.anthony.foobar.FooBar;
import com.anthony.foobar.sound.Sound;
import com.anthony.theshardcollector.shutdown.ShutdownHandler;

public class Main {
	
	public static final String GAME_NAME = "The Shard Collector";
	
	public static ShapeshiftGame instance;
	
	private static Sound currentMainLoop;
	
	public static void main(String[] args) {
		
		FooBar.init(System.getProperty("user.home") + File.separator + "Desktop", Main.GAME_NAME + "_logs");
		
		Runtime.getRuntime().addShutdownHook(new ShutdownHandler());
		
		Main.instance = new ShapeshiftGame();
		
		Main.setMainLoop(new Sound("/sounds/music.wav"), .7);
	}
	
	public static void setMainLoop(Sound s, double volume) {
		
		if (Main.currentMainLoop != null) {
			
			Main.currentMainLoop.stop();
		}
		
		s.play(volume, true);
		
		Main.currentMainLoop = s;
	}
}