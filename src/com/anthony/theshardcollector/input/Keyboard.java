package com.anthony.theshardcollector.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {
	
	public enum Key {
		
		W(KeyEvent.VK_W),
		A(KeyEvent.VK_A),
		S(KeyEvent.VK_S),
		D(KeyEvent.VK_D),
		UP(KeyEvent.VK_UP),
		LEFT(KeyEvent.VK_LEFT),
		DOWN(KeyEvent.VK_DOWN),
		RIGHT(KeyEvent.VK_RIGHT),
		ENTER(KeyEvent.VK_ENTER),
		X(KeyEvent.VK_X)
		;
		
		private int keyCode;
		
		private boolean pressed;
		
		private Key(int keyCode) {
			
			this.keyCode = keyCode;
			
			this.pressed = false;
		}
		
		public void setPressed(boolean pressed) {
			
			this.pressed = pressed;
		}
		
		public int getKeyCode() {
			
			return this.keyCode;
		}
		
		public boolean getPressed() {
			
			return this.pressed;
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		
		for (Key key : Key.values()) {
			
			if (key.getKeyCode() == e.getKeyCode()) {
				
				key.setPressed(true);
				return;
			}
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		
		for (Key key : Key.values()) {
			
			if (key.getKeyCode() == e.getKeyCode()) {
				
				key.setPressed(false);
				return;
			}
		}
	}
}