package com.anthony.theshardcollector.shutdown;

import com.anthony.foobar.FooBar;

public class ShutdownHandler extends Thread {
	
	@Override
	public void run() {
		
		FooBar.getLogger().info("The frame was closed!");
		FooBar.getLogger().info("Goodbye!");
		FooBar.getLogger().info("Made for Ludum Dare 35!");
	}
}
