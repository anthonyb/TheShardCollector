package com.anthony.theshardcollector.level.tile.material;

import com.anthony.foobar.game.graphics.sprite.Sprite;

public enum Material {
	
	VOID(0, Sprite.VOID),
	DARK_BRICK(1, Sprite.DARK_BRICK),
	LIGHT_BRICK(2, Sprite.LIGHT_BRIGHT),
	DARK_FLOOR(3, Sprite.DARK_FLOOR),
	DARK_FLOOR_ALT(4, Sprite.DARK_FLOOR_ALT),
	LIGHT_FLOOR(5, Sprite.LIGHT_FLOOR),
	LIGHT_FLOOR_ALT(6, Sprite.LIGHT_FLOOR_ALT),
	MIXED_FLOOR(7, Sprite.MIXED_FLOOR)
	;
	
	private int id;
	
	private Sprite sprite, shapeShiftSprite;
	
	private Material(int id, Sprite sprite) {
		
		this.id = id;
		
		this.sprite = sprite;
		
		this.shapeShiftSprite = sprite.getNegative();
	}
	
	public int getID() {
		
		return this.id;
	}
	
	public Sprite getSprite(boolean shapeShift) {
		
		return shapeShift ? this.shapeShiftSprite : this.sprite;
	}
}
