package com.anthony.theshardcollector.level.tile;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.theshardcollector.level.tile.material.Material;

public class Tile {
	
	public static final int STANDARD_TILE_SIZE = 16, TILE_SCALE = 2;
	
	private int x, y;
	
	private boolean shapeShift;
	
	private Material material;
	
	public Tile(int x, int y, Material material) {
		
		this.x = x;
		this.y = y;
		
		this.shapeShift = false;
		
		this.material = material;
	}
	
	public void render(Screen screen) {
		
		screen.renderTile(this.x, this.y, this.material.getSprite(shapeShift));
	}
	
	public void setShapeShift(boolean shapeShift) {
		
		this.shapeShift = shapeShift;
	}
	
	public void setMaterial(Material material) {
		
		this.material = material;
	}
	
	public int getX() {
		
		return this.x;
	}
	
	public int getY() {
		
		return this.y;
	}
	
	public boolean getShapeShift() {
		
		return this.shapeShift;
	}
	
	public Material getMaterial() {
		
		return this.material;
	}
}