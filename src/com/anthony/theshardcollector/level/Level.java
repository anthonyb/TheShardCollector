package com.anthony.theshardcollector.level;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.imageio.ImageIO;

import com.anthony.foobar.FooBar;
import com.anthony.foobar.game.entity.Entity;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.theshardcollector.entity.player.Player;
import com.anthony.theshardcollector.level.tile.Tile;
import com.anthony.theshardcollector.level.tile.material.Material;

public class Level {
	
	private String path;
	
	private int width, height;
	
	private int[] data;
	
	private Tile[] tiles;
	
	private Camera camera;
	
	private CopyOnWriteArrayList<Entity> entities;
	
	private Player player;
	
	public Level() {}
	
	public Level(String path, Camera camera) {
		
		this.path = path;
		
		this.camera = camera;
		
		this.entities = new CopyOnWriteArrayList<Entity>();
		
		this.load();
	}
	
	private void load() {
		
		try {
			
			BufferedImage image = ImageIO.read(Level.class.getResource(this.path));
			
			this.width = image.getWidth();
			this.height = image.getHeight();
			
			this.data = new int[this.width * this.height];
			this.tiles = new Tile[this.width * this.height];
			
			image.getRGB(0, 0, this.width, this.height, this.data, 0, this.width);
			
			for (int i = 0; i < this.data.length; i++) {
				
				int col = this.data[i];
				
				Material mat;
				
				switch (col) {
				case 0xFF404040:
					mat = Material.DARK_BRICK;
					break;
				case 0xFFBFBFBF:
					mat = Material.LIGHT_BRICK;
					break;
				case 0xFF191919:
					
					mat = (int) (Math.random() * 4) == 0 ? Material.DARK_FLOOR_ALT : Material.DARK_FLOOR;
					break;
				case 0xFFE6E6E6:
					
					mat = (int) (Math.random() * 4) == 0 ? Material.LIGHT_FLOOR_ALT : Material.LIGHT_FLOOR;
					break;
				case 0xFF9D9D9D:
					
					mat = Material.MIXED_FLOOR;
					break;
					default:
						mat = Material.VOID;
						break;
				}
				
				this.tiles[i] = new Tile(i % this.width, i / this.width, mat);
			}
		} catch (IOException e) {
			
			FooBar.getLogger().warn("Could not load level " + this.path + "!");
			FooBar.getLogger().warn("Exiting!");
			
			System.exit(0);
		}
	}
	
	public void tick() {
		
		for (Entity e : this.entities) {
			
			e.tick();
		}
	}
	
	public void render(Screen screen) {
		
		for (Tile tile : tiles) {
			
			tile.render(screen);
		}
		
		for (Entity e : this.entities) {
			
//			Render player last.
			if (e.equals(this.player)) {
				
				continue;
			}
			
			e.render(screen);
		}
		
		this.player.render(screen);
	}
	
	public void postRender(Graphics g) {
		
		for (Entity e : this.entities) {
			
			if (e.equals(this.player)) {
				
//				Render Player last.
				continue;
			}
			
			e.postRender(g);
		}
		
		this.player.postRender(g);
	}
	
	public void addEntity(Entity e) {
		
		this.entities.add(e);
	}
	
	public void removeEntity(Entity e) {
		
		this.entities.remove(e);
	}
	
	public void setPlayer(Player player) {
		
		this.player = player;
		
		this.addEntity(player);
	}
	
	public int getWidth() {
		
		return this.width;
	}
	
	public int getHeight() {
		
		return this.height;
	}
	
	public int[] getData() {
		
		return this.data.clone();
	}
	
	public Tile[] getTiles() {
		
		return this.tiles;
	}
	
	public Camera getCamera() {
		
		return this.camera;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entity> getEntities() {
		
		return (List<Entity>) this.entities.clone();
	}
	
	public Player getPlayer() {
		
		return this.player;
	}
}