package com.anthony.theshardcollector.level.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.theshardcollector.Main;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.particle.ParticleSpawner;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;

public class WinMenu extends Level {
	
	private static VarianceSound s = new VarianceSound("/sounds/select.wav");
	
	private int selected;
	
	private String[] options, help;
	
	private ParticleSpawner ps;
	
	public WinMenu() {
		
		super();
		
		this.selected = 0;
		
		this.options = new String[]{"Play Again", "Quit"};
		this.help = new String[]{
			
			"You have beaten " + Main.GAME_NAME + "!",
			"",
			"Created by LudumAnthony",
			"in 48 hours for Ludum Dare 35.",
			"",
			"Theme: Shapeshift",
		};
		
		this.ps = new ParticleSpawner(0, -Sprite.CONFETTI.getHeight(), Main.instance.getWidth(), 1, -1, 470, 1, Sprite.CONFETTI, this, Direction.SOUTH);
	}
	
	@Override
	public void tick() {
		
		if (Key.W.getPressed() && this.selected == 1) {
			
			WinMenu.s.getPrimarySound().play(.3, false);
			this.selected = 0;
		}
			
		if (Key.S.getPressed() && this.selected == 0) {
			
			WinMenu.s.getPrimarySound().play(.3, false);
			this.selected = 1;
		}
		
		if (Key.ENTER.getPressed()) {
			
			switch (this.selected) {
			case 0:
				
				Main.instance.setLevel(new Level("/levels/level1.png", Main.instance.getCamera()));
				Main.instance.setUpFirstLevelEntities();
				break;
			case 1:
				
				System.exit(0);
				break;
				default:
					break;
			}
		}
		
		this.ps.tick();
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.fill(0xFFFDEEDF);
		
		this.ps.render(screen);
	}
	
	@Override
	public void postRender(Graphics g) {
		
		g.setFont(new Font("Verdana", 1, 35));
		
		g.setColor(Color.BLACK);
		g.drawString("Congratulations!", 225, 50);
		
		g.setFont(new Font("Times New Roman", 0, 20));
		
		for (int i = 0; i < this.help.length; i++) {
			
			g.drawString(this.help[i], 280, 150 + (i * 25));
		}
		
		g.setFont(new Font("Verdana", 1, 20));
		
		for (int i = 0; i < this.options.length; i++) {
			
			String option = this.options[i];
			
			g.setColor(Color.BLACK);
			
			if (selected == i) {
				
				g.setColor(Color.GRAY);
				g.drawString(">", 300, 415 + (22 * i));
				g.setColor(Color.CYAN);
			}
			
			g.drawString(option, 330, 415 + (22 * i));
		}
	}
}