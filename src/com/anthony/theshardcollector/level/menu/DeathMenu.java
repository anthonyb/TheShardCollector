package com.anthony.theshardcollector.level.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.AnimatedSprite;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.sound.Sound;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.theshardcollector.Main;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.particle.ParticleSpawner;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;

public class DeathMenu extends Level {
	
//	TODO maybe do taunts?
	private static VarianceSound s = new VarianceSound("/sounds/select.wav");
	
	private static String[] taunts = new String[]{
		
		"That was terrible...",
		"You have to be quicker than that.",
		"TIP: Enter the Konami Code for a free health potion!",
		"Why are you so bad?",
		"It\'s really not that hard!",
		"Did you win!?! ...oh."
	};
	
	private final static Color PINK = new Color(0xFFFF66FF);
	
	private int selected, wTimer, sTimer;
	
	private String taunt;
	
	private String[] options;
	
	private AnimatedSprite animSprite, animSpriteFaster;
	
	private ParticleSpawner ps;
	
	public DeathMenu() {
		
		super();
		
		new Sound("/sounds/dead.wav").play(.7, false);
		
		this.selected = this.wTimer = this.sTimer = 0;
		
		this.taunt = DeathMenu.taunts[(int) (Math.random() * DeathMenu.taunts.length)];
		
		this.options = new String[]{"Play Again", "Help", "Quit"};
		
		this.animSprite = new AnimatedSprite(
			
			10, true,
			Sprite.WITHERED_PARTICLE_0,
			Sprite.WITHERED_PARTICLE_1,
			Sprite.WITHERED_PARTICLE_2,
			Sprite.WITHERED_PARTICLE_3,
			Sprite.WITHERED_PARTICLE_4,
			Sprite.WITHERED_PARTICLE_5
		);
		
		this.animSpriteFaster = new AnimatedSprite(
			
			3, true,
			Sprite.WITHERED_PARTICLE_0,
			Sprite.WITHERED_PARTICLE_1,
			Sprite.WITHERED_PARTICLE_2,
			Sprite.WITHERED_PARTICLE_3,
			Sprite.WITHERED_PARTICLE_4,
			Sprite.WITHERED_PARTICLE_5
		);
		
		this.ps = new ParticleSpawner(280, Main.instance.getHeight(), 150, 1, -1, 100, 10, Sprite.MUTATE_LINE_PARTICLE, this, Direction.NORTH);
	}
	
	@Override
	public void tick() {
		
		this.animSprite.tick();
		this.animSpriteFaster.tick();
		
		if (Key.W.getPressed()) {
			
			if (this.selected > 0 && this.wTimer == 0) {
			
				DeathMenu.s.getPrimarySound().play(.3, false);
				this.selected--;
				this.wTimer = 30;
			}
		} else {
			
			this.wTimer = 0;
		}
		
		if (Key.S.getPressed()) {
			
			if (this.selected < options.length - 1 && this.sTimer == 0) {
				
				DeathMenu.s.getPrimarySound().play(.3, false);
				this.selected++;
				this.sTimer = 30;
			}
		} else {
			
			this.sTimer = 0;
		}
		
		if (Key.ENTER.getPressed()) {
			
			switch (this.selected) {
			case 0:
				
				
				Main.instance.setLevel(new Level("/levels/level1.png", Main.instance.getCamera()));
				Main.instance.setUpFirstLevelEntities();
				break;
			case 1:
				
				Main.instance.setLevel(new HelpMenu("Play Again"));
				return;
			case 2:
				
				System.exit(0);
				break;
				default:
					break;
			}
		}
		
		if (this.wTimer > 0) {
			
			this.wTimer--;
		}
		
		if (this.sTimer > 0) {
			
			this.sTimer--;
		}
		
		this.ps.tick();
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.fill(0xFFFDEEDF);
		
		this.ps.render(screen);
		
		screen.renderSprite(210, 20, 15, true, this.animSprite.getSprite());
		screen.renderSprite(100, 20, 5, true, this.animSpriteFaster.getSprite());
		screen.renderSprite(520, 20, 5, true, this.animSpriteFaster.getSprite());
	}
	
	@Override
	public void postRender(Graphics g) {
		
		g.setFont(new Font("Verdana", 2, 35));
		
		g.setColor(Color.BLACK);
		g.drawString("You Died!", 280, 340);
		
		g.setFont(new Font("Verdana", 1, 20));
		
		g.setColor(DeathMenu.PINK);
		this.drawCenteredString(this.taunt, Main.instance.getWidth(), 370, g);
		
		for (int i = 0; i < options.length; i++) {
			
			String option = options[i];
			
			g.setColor(Color.BLACK);
			
			if (selected == i) {
				
				g.setColor(Color.GRAY);
				g.drawString(">", 300, 400 + (22 * i));
				g.setColor(Color.CYAN);
			}
			
			g.drawString(option, 330, 400 + (22 * i));
		}
	}
	
	private void drawCenteredString(String s, int w, int h, Graphics g) {
		
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		g.drawString(s, x, h);
	}
}