package com.anthony.theshardcollector.level.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.AnimatedSprite;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.game.utils.UpDownInt;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.theshardcollector.Main;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;

public class MainMenu extends Level {
	
	private static VarianceSound s = new VarianceSound("/sounds/select.wav");
	
	private int selected, wTimer, sTimer;
	private String[] options;
	
	private UpDownInt upDownY;
	
	private AnimatedSprite animSprite;
	
	public MainMenu(int height) {
		
		super();
		
		this.selected = this.wTimer = this.sTimer = 0;
		this.options = new String[]{"Play", "Help", "Quit"};
		
		this.upDownY = new UpDownInt(5, -10, +10);
		
		this.animSprite = new AnimatedSprite(
			20, false,
			Sprite.M0_FORWARD_1, Sprite.M0_FORWARD_1, Sprite.M0_FORWARD_2, Sprite.M0_FORWARD_1, Sprite.M0_FORWARD_1, Sprite.M0_FORWARD_2, Sprite.M0_FORWARD_1, Sprite.M0_FORWARD_1,
			Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_2, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_1, Sprite.M1_FORWARD_3, Sprite.M1_FORWARD_1,
			Sprite.M2_RIGHT_1, Sprite.M2_RIGHT_2, Sprite.M2_RIGHT_3, Sprite.M2_RIGHT_1, Sprite.M2_RIGHT_2, Sprite.M2_RIGHT_3, Sprite.M2_RIGHT_1, Sprite.M2_RIGHT_2, Sprite.M2_RIGHT_3, Sprite.M2_RIGHT_1, Sprite.M2_RIGHT_2, Sprite.M2_RIGHT_3
		);
	}
	
	@Override
	public void tick() {
		
		this.animSprite.tick();
		
		this.upDownY.tick();
		
		if (Key.W.getPressed()) {
			
			if (this.selected > 0 && this.wTimer == 0) {
			
				MainMenu.s.getPrimarySound().play(.3, false);
				this.selected--;
				this.wTimer = 30;
			}
		} else {
			
			this.wTimer = 0;
		}
		
		if (Key.S.getPressed()) {
			
			if (this.selected < options.length - 1 && this.sTimer == 0) {
				
				MainMenu.s.getPrimarySound().play(.3, false);
				this.selected++;
				this.sTimer = 30;
			}
		} else {
			
			this.sTimer = 0;
		}
		
		if (Key.ENTER.getPressed()) {
			
			switch (this.selected) {
			case 0:
				
				
				Main.instance.setLevel(new Level("/levels/level1.png", Main.instance.getCamera()));
				Main.instance.setUpFirstLevelEntities();
				break;
			case 1:
				
				Main.instance.setLevel(new HelpMenu("Play"));
				return;
			case 2:
				
				System.exit(0);
				break;
				default:
					break;
			}
		}
		
		if (this.wTimer > 0) {
			
			this.wTimer--;
		}
		
		if (this.sTimer > 0) {
			
			this.sTimer--;
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.fill(0xFFFDEEDF);
		
		screen.renderBrightnessSprite(50, false, 240, 50 + this.upDownY.get(), 8, true, this.animSprite.getSprite());
	}
	
	@Override
	public void postRender(Graphics g) {
		
		g.setFont(new Font("Verdana", 1, 35));
		
		g.setColor(Color.BLACK);
		g.drawString(Main.GAME_NAME, 180, 340);
		
		g.setFont(new Font("Verdana", 1, 25));
		
		for (int i = 0; i < options.length; i++) {
			
			String option = options[i];
			
			g.setColor(Color.BLACK);
			
			if (selected == i) {
				
				g.setColor(Color.GRAY);
				g.drawString(">", 300, 370 + (30 * i));
				g.setColor(Color.CYAN);
			}
			
			g.drawString(option, 330, 370 + (30 * i));
		}
	}
}