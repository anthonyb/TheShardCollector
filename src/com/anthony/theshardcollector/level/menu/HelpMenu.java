package com.anthony.theshardcollector.level.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.sound.VarianceSound;
import com.anthony.theshardcollector.Main;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.particle.ParticleSpawner;
import com.anthony.theshardcollector.input.Keyboard.Key;
import com.anthony.theshardcollector.level.Level;

public class HelpMenu extends Level {
	
	private static VarianceSound s = new VarianceSound("/sounds/select.wav");
	
	private int initialScreenDelay, selected;
	
	private String playText;
	
	private String[] options, help;
	
	private ParticleSpawner ps;
	
	public HelpMenu(String playText) {
		
		super();
		
		this.initialScreenDelay = 50;
		this.selected = 0;
		
		this.playText = playText;
		
		this.options = new String[]{this.playText, "Quit"};
		this.help = new String[]{
			
			"W Move Up",
			"A Move Left",
			"S Move Down",
			"D Move Right",
			"\u2190 \u2192 \u2191 \u2193 Shoot Projectile",
			"When the floor is black:",
			"    You cannot walk through black walls.",
			"When the floor is white:",
			"    You cannot walk through white walls.",
			"If a wall is gray:",
			"    You can never walk through it.",
			"When the Shard counter is yellow:",
			"    You can mutate using an available pedestal."
		};
		
		this.ps = new ParticleSpawner(280, Main.instance.getHeight(), 150, 1, -1, 50, 10, Sprite.MUTATE_LINE_PARTICLE, this, Direction.NORTH);
	}
	
	@Override
	public void tick() {
		
		if (this.initialScreenDelay > 0) {
			
			this.initialScreenDelay--;
			return;
		}
		
		if (Key.W.getPressed() && this.selected == 1) {
			
			HelpMenu.s.getPrimarySound().play(.3, false);
			this.selected = 0;
		}
			
		if (Key.S.getPressed() && this.selected == 0) {
			
			HelpMenu.s.getPrimarySound().play(.3, false);
			this.selected = 1;
		}
		
		if (Key.ENTER.getPressed()) {
			
			switch (this.selected) {
			case 0:
				
				Main.instance.setLevel(new Level("/levels/level1.png", Main.instance.getCamera()));
				Main.instance.setUpFirstLevelEntities();
				break;
			case 1:
				
				System.exit(0);
				break;
				default:
					break;
			}
		}
		
		this.ps.tick();
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.fill(0xFFFDEEDF);
		
		this.ps.render(screen);
	}
	
	@Override
	public void postRender(Graphics g) {
		
		g.setFont(new Font("Verdana", 1, 35));
		
		g.setColor(Color.BLACK);
		g.drawString("Controls:", 285, 50);
		
		g.setFont(new Font("Times New Roman", 0, 20));
		
		for (int i = 0; i < this.help.length; i++) {
			
			g.drawString(this.help[i], 310, 80 + (i * 25));
		}
		
		g.setFont(new Font("Verdana", 1, 20));
		
		for (int i = 0; i < this.options.length; i++) {
			
			String option = this.options[i];
			
			g.setColor(Color.BLACK);
			
			if (selected == i) {
				
				g.setColor(Color.GRAY);
				g.drawString(">", 300, 415 + (22 * i));
				g.setColor(Color.CYAN);
			}
			
			g.drawString(option, 330, 415 + (22 * i));
		}
	}
}
