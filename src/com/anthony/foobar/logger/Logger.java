package com.anthony.foobar.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import javax.swing.JOptionPane;

import com.anthony.foobar.event.Event;
import com.anthony.foobar.event.Listen;

public class Logger {
	
	private String fileSeparator, lineSeparator;
	
	private String dataFolderLocation;
	private String startUpTime;
	
	private Calendar calendar;
	
	private File dataFolder;
	private File currentLog;
	
	private FileWriter fileWriter;
	
	private PrintWriter printWriter;
	
	public Logger(String locationFolderName, String folderName) {
		
		this.fileSeparator = File.separator;
		this.lineSeparator = System.getProperty("line.separator");
		
		this.dataFolderLocation = locationFolderName + this.fileSeparator + folderName;
		
		this.dataFolder = new File(this.dataFolderLocation);
		
		if (!this.dataFolder.exists()) {
			
			this.dataFolder.mkdir();
		}
		
		this.calendar = Calendar.getInstance();
		
		this.startUpTime = this.calendar.getTime().toString().replaceAll(" ", "_").replaceAll(":", "-");
		
		this.currentLog = new File(this.dataFolder.getAbsolutePath() + this.fileSeparator + this.startUpTime + ".txt");
		
		if (this.currentLog.exists()) {
			
			JOptionPane.showMessageDialog(null, "The program was terminated because a log file with the current time already exists!  Did we go back in time?");
			System.exit(0);
			return;
		}
		
		try {
			
			this.currentLog.createNewFile();
		} catch (IOException e) {
			
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "The program was terminated because a log file with the current time could not be created.");
			System.exit(0);
			return;
		}
		
		try {
			
			this.fileWriter = new FileWriter(this.currentLog);
			this.printWriter = new PrintWriter(fileWriter);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		this.write(this.startUpTime);
		this.info("Logger was created!");
	}
	
	public void destroy() {
		
		try {
			
			this.fileWriter.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		this.printWriter.close();
	}
	
	public void info(String message) {
		
		String newMessage = "[INFO] [" + this.calendar.get(Calendar.HOUR_OF_DAY) + "h:" + this.calendar.get(Calendar.MINUTE) + "m:" + this.calendar.get(Calendar.SECOND) + "s:" + this.calendar.get(Calendar.MILLISECOND) + "ms] " + message;
		
		this.write(newMessage);
	}
	
	public void warn(String message) {
		
		String newMessage = "[WARN] [" + this.calendar.get(Calendar.HOUR_OF_DAY) + "h:" + this.calendar.get(Calendar.MINUTE) + "m:" + this.calendar.get(Calendar.SECOND) + "s:" + this.calendar.get(Calendar.MILLISECOND) + "ms] " + message;
		
		this.write(newMessage);
	}
	
	private void write(String message) {
		
		this.printWriter.write(message + this.lineSeparator);
		
		this.printWriter.flush();
		
		System.out.println(message);
	}
	
	@Listen(priority = 0)
	public void receive(Event e) {
		
		this.write("[EVENT] " + e.getID() + " | " + e.toString());
	}
}