package com.anthony.foobar.game.entity;

import java.awt.Rectangle;
import java.util.Random;

import com.anthony.foobar.game.entity.type.Damageable;
import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.theshardcollector.direction.Direction;
import com.anthony.theshardcollector.entity.loot.Shard;
import com.anthony.theshardcollector.entity.particle.ParticleSpawner;
import com.anthony.theshardcollector.entity.projectile.Projectile;
import com.anthony.theshardcollector.level.Level;
import com.anthony.theshardcollector.level.tile.Tile;
import com.anthony.theshardcollector.level.tile.material.Material;

public abstract class Mob extends Entity implements Damageable {
	
	protected int damageTimer, health, maxDropShards, maxHealth, scale, addToXRect, addToYRect, addToWidthRect, addToHeightRect;
	
	protected Rectangle collisionRect;
	
	protected String identifier;
	
	protected Sprite sprite;
	
	public Mob(int x, int y, int maxDropShardsPlusOne, int maxHealth, Level level, Rectangle collisionRect, String identifier, Sprite sprite) {
		
		super(x, y, level);
		
		this.damageTimer = 0;
		
		this.health = this.maxHealth = maxHealth;
		
		this.maxDropShards = maxDropShardsPlusOne;
		
		this.addToXRect = this.addToYRect = this.addToWidthRect = this.addToHeightRect = 0;
		
		this.scale = 1;
		
		this.collisionRect = collisionRect;
		
		this.identifier = identifier;
		
		this.sprite = sprite;
	}
	
	public abstract boolean hit(Projectile proj);
	
	@Override
	public void tick() {
		
		if (this.damageTimer != 0) {
			
			this.damageTimer--;
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		if (damageTimer < 5) {
			
			screen.renderSprite(this.x, this.y, this.scale, false, this.sprite);
		} else {
			
			screen.renderBlendSprite(super.x, super.y, this.scale, false, false, 0xFFFF0000, 0.5F, this.sprite);
		}
	}
	
	@Override
	public void remove() {
		
		super.level.removeEntity(this);
	}
	
	@Override
	public int getHealth() {
		
		return this.health;
	}
	
	@Override
	public int getMaxHealth() {
		
		return this.maxHealth;
	}
	
	@Override
	public void kill() {
		
		Random random = new Random();
		
		int scaledWidth = this.scale * this.sprite.getWidth(), scaledHeight = this.scale * this.sprite.getHeight();
		
		for (int i = 0; i < random.nextInt(this.maxDropShards) + 1; i++) {
			
			super.level.addEntity(new Shard(super.x + random.nextInt(scaledWidth), super.y + random.nextInt(scaledHeight), super.level));
		}
		
		super.level.addEntity(new ParticleSpawner(super.x, super.y, scaledWidth, scaledHeight, 125 / 2, 50, 20, Sprite.DUST_PARTICLE, super.level, Direction.NORTH));
		
		this.remove();
	}
	
	@Override
	public void setHealth(int health) {
		
		if (health > this.health) {
			
			this.health = health;
			return;
		}
		
		if (this.damageTimer != 0) {
			
			return;
		}
		
		this.health = health;
		
		if (this.health == 0) {
			
			this.kill();
			return;
		}
		
		this.damageTimer = 50;
	}
	
	@Override
	public void setMaxHealth(int maxHealth) {
		
		this.maxHealth = maxHealth;
	}
	
	public void move(int xa, int ya) {
		
		x += xa;
		y += ya;
		
		this.updateCollisionRect();
	}
	
	public void updateCollisionRect() {
		
		this.collisionRect.setBounds(super.x + this.addToXRect, super.y + this.addToYRect, this.scale * this.sprite.getWidth() + this.addToWidthRect, this.scale * this.sprite.getHeight() + this.addToHeightRect);
	}
	
	public boolean isColliding(int xa, int ya) {
		
		double xaDir = xa > 0 ? +.33 : xa == 0 ? 0 : -.33;
		double yaDir = ya > 0 ? +.33 : ya == 0 ? 0 : -.33;
		
		int xMid = super.x + this.addToXRect + ((this.sprite.getWidth() * this.scale + this.addToWidthRect) / 2);
		int yMid = super.y + this.addToYRect + ((this.sprite.getHeight() * this.scale + this.addToHeightRect) / 2);
		
		int tileX = ((xMid + (int) (xaDir * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE)) / (Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE));
		int tileY = (((yMid + (int) (yaDir * Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE)) / (Tile.STANDARD_TILE_SIZE * Tile.TILE_SCALE)));
		int tileYinX = super.level.getWidth() * tileY;
		
		if (tileX > 0 && tileY > 0 && tileYinX > 0 && tileX + tileYinX < super.level.getTiles().length) {
			
			Tile movingTowards = super.level.getTiles()[tileX + tileYinX];
			
			if (movingTowards.getMaterial() == Material.MIXED_FLOOR) {
				
				return true;
			}
			
			if (movingTowards.getShapeShift()) {
				
				if (movingTowards.getMaterial() == Material.LIGHT_BRICK) {
					
					return true;
				}
			} else {
				
				if (movingTowards.getMaterial() == Material.DARK_BRICK) {
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public int getX() {
		
		return this.x;
	}
	
	public int getY() {
		
		return this.y;
	}
	
	public Rectangle getCollisionRect() {
		
		return this.collisionRect;
	}
	
	public String getIdentifier() {
		
		return this.identifier;
	}
}