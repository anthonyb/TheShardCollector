package com.anthony.foobar.game.entity;

import java.awt.Graphics;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.theshardcollector.level.Level;

public abstract class Entity {
	
	protected int x, y;
	
	protected Level level;
	
	public Entity(int x, int y, Level level) {
		
		this.x = x;
		this.y = y;
		
		this.level = level;
	}
	
	public abstract void tick();
	public abstract void render(Screen screen);
	public abstract void postRender(Graphics g);
	
	public abstract void remove();
}