package com.anthony.foobar.game.graphics.sprite;

import java.awt.Color;

import com.anthony.foobar.utils.ColorHandler;
import com.anthony.theshardcollector.level.tile.Tile;

public class Sprite {
	
//	Added to base for Game
	public static final Sprite
		
		VOID = new Sprite(0, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		DARK_BRICK = new Sprite(1, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		LIGHT_BRIGHT = new Sprite(2, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		DARK_FLOOR = new Sprite(3, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		DARK_FLOOR_ALT = new Sprite(4, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		LIGHT_FLOOR = new Sprite(5, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		LIGHT_FLOOR_ALT = new Sprite(6, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		MIXED_FLOOR = new Sprite(7, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.TILES_SHEET),
		
		M0_RIGHT_1 = new Sprite(0, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		M0_RIGHT_2 = new Sprite(4, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M0_LEFT_1 = new Sprite(1, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		M0_LEFT_2 = new Sprite(5, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M0_FORWARD_1 = new Sprite(2, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		M0_FORWARD_2 = new Sprite(6, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M0_BACK_1 = new Sprite(3, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		M0_BACK_2 = new Sprite(7, 0, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M1_RIGHT_1 = new Sprite(0, 1, 32, 32, SpriteSheet.ENTITY_SHEET),
		M1_RIGHT_2 = new Sprite(2, 2, 32, 32, SpriteSheet.ENTITY_SHEET),
		M1_RIGHT_3 = new Sprite(3, 2, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M1_LEFT_1 = new Sprite(1, 1, 32, 32, SpriteSheet.ENTITY_SHEET),
		M1_LEFT_2 = new Sprite(4, 2, 32, 32, SpriteSheet.ENTITY_SHEET),
		M1_LEFT_3 = new Sprite(5, 2, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M1_FORWARD_1 = new Sprite(2, 1, 32, 32, SpriteSheet.ENTITY_SHEET),
		M1_FORWARD_2 = new Sprite(0, 2, 32, 32, SpriteSheet.ENTITY_SHEET),
		M1_FORWARD_3 = new Sprite(1, 2, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M1_BACK = new Sprite(3, 1, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M2_RIGHT_1 = new Sprite(0, 3, 32, 32, SpriteSheet.ENTITY_SHEET),
		M2_RIGHT_2 = new Sprite(1, 3, 32, 32, SpriteSheet.ENTITY_SHEET),
		M2_RIGHT_3 = new Sprite(2, 3, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		M2_LEFT_1 = new Sprite(5, 3, 32, 32, SpriteSheet.ENTITY_SHEET),
		M2_LEFT_2 = new Sprite(4, 3, 32, 32, SpriteSheet.ENTITY_SHEET),
		M2_LEFT_3 = new Sprite(3, 3, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		MONSTER_RIGHT_1 = new Sprite(0, 5, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_RIGHT_2 = new Sprite(0, 6, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_RIGHT_3 = new Sprite(0, 7, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		MONSTER_LEFT_1 = new Sprite(1, 5, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_LEFT_2 = new Sprite(1, 6, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_LEFT_3 = new Sprite(1, 7, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		MONSTER_FORWARD_1 = new Sprite(2, 5, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_FORWARD_2 = new Sprite(2, 6, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_FORWARD_3 = new Sprite(2, 7, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		MONSTER_BACK_1 = new Sprite(3, 5, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_BACK_2 = new Sprite(3, 6, 32, 32, SpriteSheet.ENTITY_SHEET),
		MONSTER_BACK_3 = new Sprite(3, 7, 32, 32, SpriteSheet.ENTITY_SHEET),
		
		MUTATE_LINE_PARTICLE = new Sprite(0, 0, 20, 20, SpriteSheet.PARTICLES_SHEET),
		MUTATE_DUST_PARTICLE = new Sprite(1, 0, 20, 20, SpriteSheet.PARTICLES_SHEET),
		BLOOD_DUST_PARTICLE = new Sprite(2, 0, 20, 20, SpriteSheet.PARTICLES_SHEET),
		DUST_PARTICLE = new Sprite(3, 0, 20, 20, SpriteSheet.PARTICLES_SHEET),
		DUST_RED_PARTICLE = new Sprite(4, 0, 20, 20, SpriteSheet.PARTICLES_SHEET),
		WITHERED_PARTICLE_0 = new Sprite(0, 1, 20, 20, SpriteSheet.PARTICLES_SHEET),
		WITHERED_PARTICLE_1 = new Sprite(1, 1, 20, 20, SpriteSheet.PARTICLES_SHEET),
		WITHERED_PARTICLE_2 = new Sprite(2, 1, 20, 20, SpriteSheet.PARTICLES_SHEET),
		WITHERED_PARTICLE_3 = new Sprite(3, 1, 20, 20, SpriteSheet.PARTICLES_SHEET),
		WITHERED_PARTICLE_4 = new Sprite(4, 1, 20, 20, SpriteSheet.PARTICLES_SHEET),
		WITHERED_PARTICLE_5 = new Sprite(0, 2, 20, 20, SpriteSheet.PARTICLES_SHEET),
		CONFETTI = new Sprite(1, 2, 20, 20, SpriteSheet.PARTICLES_SHEET),
		
		SLIME_BALL_PROJECTILE = new Sprite(0, 0, 20, 20, SpriteSheet.PROJECTILES_SHEET),
		TEAR_BALL_PROJECTILE = new Sprite(1, 0, 20, 20, SpriteSheet.PROJECTILES_SHEET),
		POOP_BALL_PROJECTILE = new Sprite(2, 0, 20, 20, SpriteSheet.PROJECTILES_SHEET),
		ENEMY_PROJECTILE = new Sprite(3, 0, 20, 20, SpriteSheet.PROJECTILES_SHEET),
		
		SHARD = new Sprite(0, 0, 20, 20, new SpriteSheet("/shard/shard.png")),
		
		PEDESTAL_1 = new Sprite(0, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.PEDESTAL_SHEET),
		PEDESTAL_2 = new Sprite(0, 1, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.PEDESTAL_SHEET),
		PEDESTAL_3 = new Sprite(0, 2, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.PEDESTAL_SHEET),
		
		PEDESTAL_RED_1 = new Sprite(1, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.PEDESTAL_SHEET),
		PEDESTAL_RED_2 = new Sprite(1, 1, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.PEDESTAL_SHEET),
		PEDESTAL_RED_3 = new Sprite(1, 2, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, SpriteSheet.PEDESTAL_SHEET),
		
		HEALTH_BAR = new Sprite(0, 0, 100, 10, new SpriteSheet("/gui/health_bar.png")),
		
		HEALTH_POTION = new Sprite(0, 0, 16, 16, SpriteSheet.ITEMS_SHEET),
		
		DOOR = new Sprite(0, 0, Tile.STANDARD_TILE_SIZE, Tile.STANDARD_TILE_SIZE, new SpriteSheet("/door/door.png"))
	;
//	---
	
	private int x, y, width, height;
	
	public int[] pixels;
	
	private SpriteSheet sheet;
	
	public Sprite(int x, int y, int width, int height, SpriteSheet sheet) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.pixels = new int[width * height];
		
		this.sheet = sheet;
		
		this.load();
	}
	
	public Sprite(Sprite sprite, Color[] greyscale, Color[] colored) {
		
		this.x = sprite.x;
		this.y = sprite.y;
		this.width = sprite.getWidth();
		this.height = sprite.getHeight();
		
		this.pixels = new int[width * height];
		
		this.sheet = sprite.getSpriteSheet();
		
		this.load(greyscale, colored);
	}
	
	public Sprite(Sprite sprite, boolean rotDown, boolean rotRight, boolean rotLeft) {
		
		this.x = sprite.x;
		this.y = sprite.y;
		this.width = sprite.width;
		this.height = sprite.height;
		
		this.sheet = sprite.sheet;
		
		this.pixels = sprite.pixels.clone();
		
		int[][] matrix = new int[this.height][this.width];
		
		for (int i = 0; i < this.height; i++) {
			
			for (int j = 0; j < this.width; j++) {
				
				matrix[i][j] = this.pixels[(i * this.width) + j];
			}
		}
		
		if (rotRight || rotDown) {
			
			/*
			 * NOTE: Is the system backwards?
			 * I rotate left to go right, and rotate right to go left.
			 * TODO investigate.
			 *
			 * [0,0] [1,0] [2,0] [3,0]
			 * [0,1] [1,1] [2,1] [3,1]
			 * [0,2] [1,2] [2,2] [3,2]
			 * [0,3] [1,3] [2,3] [3,3]
			 * 
			 * [3,0] [3,1] [3,2] [3,3]
			 * [2,0] [2,1] [2,2] [2,3]
			 * [1,0] [1,1] [1,2] [1,3]
			 * [0,0] [0,1] [0,2] [0,3]
			 */
			
			for (int repeat = 0; repeat < (rotDown ? 2 : 1); repeat++) {
				
				int[][] matrixClone = new int[this.height][this.width];
				
				for (int i = 0; i < this.height; i++) {
					
					for (int j = 0; j < this.width; j++) {
						
						matrixClone[i][j] = matrix[i][j];
					}
				}
				
				for (int i = 0; i < this.height; i++) {
					
					for (int j = 0; j < this.width; j++) {
						
						int newY = this.height - (j + 1);
						int newX = i;
						
						matrix[i][j] = matrixClone[newY][newX];
					}
				}
			}
		}
		
		if (rotLeft) {
			
			/*
			 * [0,0] [1,0] [2,0] [3,0]
			 * [0,1] [1,1] [2,1] [3,1]
			 * [0,2] [1,2] [2,2] [3,2]
			 * [0,3] [1,3] [2,3] [3,3]
			 * 
			 * [0,3] [0,2] [0,1] [0,0]
			 * [1,3] [1,2] [1,1] [1,0]
			 * [2,3] [2,2] [2,1] [2,0]
			 * [3,3] [3,2] [3,1] [3,0]
			 */
			
			int[][] matrixClone = new int[this.height][this.width];
			
			for (int i = 0; i < this.height; i++) {
				
				for (int j = 0; j < this.width; j++) {
					
					matrixClone[i][j] = matrix[i][j];
				}
			}
			
			for (int i = 0; i < this.height; i++) {
				
				for (int j = 0; j < this.width; j++) {
					
					int newY = j;
					int newX = this.width - (i + 1);
					
					matrix[i][j] = matrixClone[newY][newX];
				}
			}
		}
		
		for (int i = 0; i < this.height; i++) {
			
			for (int j = 0; j < this.width; j++) {
				
				this.pixels[(i * this.width) + j] = matrix[i][j];
			}
		}
	}
	
	private void load() {
		
		for (int y = 0; y < this.height; y++) {
			
			for (int x = 0; x < this.width; x++) {
				
				this.pixels[x + y * this.width] = this.sheet.pixels[(x + (this.x * this.width)) + (y + (this.y * this.width)) * this.sheet.width];
			}
		}
	}
	
	private void load(Color[] greyscale, Color[] colored) {
		
		for (int y = 0; y < this.height; y++) {
			
			for (int x = 0; x < this.width; x++) {
				
				int col = this.sheet.pixels[(x + (this.x * this.width)) + (y + (this.y * this.width)) * this.sheet.width];
				
				for (int i = 0; i < greyscale.length; i++) {
					
					Color c = greyscale[i];
					
					if (col == c.getRGB()) {
						
						col = colored[i].getRGB();
					}
				}
				
				this.pixels[x + y * this.width] = col;
			}
		}
	}
	
	public int getWidth() {
		
		return this.width;
	}
	
	public int getHeight() {
		
		return this.height;
	}
	
	public SpriteSheet getSpriteSheet() {
		
		return this.sheet;
	}
	
	public Sprite getNegative() {
		
		Sprite spr = new Sprite(this.x, this.y, this.width, this.height, this.sheet);
		
		for (int i = 0; i < spr.pixels.length; i++) {
			
			spr.pixels[i] = ColorHandler.negative(spr.pixels[i]);
		}
		
		return spr;
	}
}