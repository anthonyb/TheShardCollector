package com.anthony.foobar.game.graphics.screen;

import com.anthony.foobar.game.Game;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.game.utils.UpDownInt;
import com.anthony.foobar.threed.camera.Camera;
import com.anthony.foobar.utils.ColorHandler;
import com.anthony.theshardcollector.level.tile.Tile;

public class Screen {
	
	private Game game;
	
	private Camera camera;
	
	private int[] pixels;
	
	private UpDownInt brightness;
	
	public Screen(Game game, Camera camera) {
		
		this.game = game;
		
		this.camera = camera;
		
		this.pixels = new int[this.game.getWidth() * this.game.getHeight()];
		
		for (int i = 0; i < this.pixels.length; i++) {
			
			this.pixels[i] = 0xFF000000;
		}
		
		this.brightness = new UpDownInt(10, -75, 50);
	}
	
	public void tick() {
		
		this.brightness.tick();
	}
	
	public void clear() {
		
		for (int i = 0; i < this.pixels.length; i++) {
			
			this.pixels[i] = 0xFF000000;
		}
	}
	
	public void renderSprite(int xVal, int yVal, Sprite sprite) {
		
		this.renderSprite(xVal, yVal, 1, false, sprite);
	}
	
	
	public void renderSprite(int xVal, int yVal, int scale, boolean fixed, Sprite sprite) {
		
		this.renderSprite(xVal, yVal, scale, false, fixed, sprite);
	}
	
	public void renderSprite(int xVal, int yVal, int scale, boolean samePixelAmount, boolean fixed, Sprite sprite) {
		
		if (!fixed) {
		
			int cameraX = (int) this.camera.getX(), cameraY = (int) this.camera.getY();
			
			if (
					xVal + (sprite.getWidth() * scale) < cameraX
					|| yVal + (sprite.getHeight() * scale) < cameraY
					|| xVal > cameraX + this.camera.getScreenWidth()
					|| yVal > cameraY + this.camera.getScreenHeight()
				) {
				
				return;
			}
			
			xVal -= this.camera.getX();
			yVal -= this.camera.getY();
		}
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			for (int yScale = 0; samePixelAmount ? yScale == 0 : yScale < scale; yScale++) {
				
				int ya = yVal + (y * scale) + (samePixelAmount ? 0 : yScale);
				
				for (int x = 0; x < sprite.getWidth(); x++) {
					
					for (int xScale = 0; samePixelAmount ? xScale == 0 : xScale < scale; xScale++) {
						
						int xa = xVal + (x * scale) + (samePixelAmount ? 0 : xScale);
						
						int col = sprite.pixels[x + y * sprite.getWidth()];
						
						if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
							
							continue;
						}
						
						if (col != 0xffff00ff) {
							
//							Slime green
							if (col == 0xFF00FF90 || col == 0xFF00A85C) {
								
								col = ColorHandler.blend(col, this.pixels[xa + ya * this.game.getWidth()], .3F);
							}
							
							col = ColorHandler.changeBrightness(col, this.brightness.get());
							
							this.pixels[xa + ya * this.game.getWidth()] = col;
						}
					}
				}
			}
		}
	}
	
	public void renderBlendSprite(int xVal, int yVal, int scale, boolean samePixelAmount, boolean fixed, int blendColor, float ratio, Sprite sprite) {
		
		if (!fixed) {
		
			int cameraX = (int) this.camera.getX(), cameraY = (int) this.camera.getY();
			
			if (
					xVal + (sprite.getWidth() * scale) < cameraX
					|| yVal + (sprite.getHeight() * scale) < cameraY
					|| xVal > cameraX + this.camera.getScreenWidth()
					|| yVal > cameraY + this.camera.getScreenHeight()
				) {
				
				return;
			}
			
			xVal -= this.camera.getX();
			yVal -= this.camera.getY();
		}
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			for (int yScale = 0; samePixelAmount ? yScale == 0 : yScale < scale; yScale++) {
				
				int ya = yVal + (y * scale) + (samePixelAmount ? 0 : yScale);
				
				for (int x = 0; x < sprite.getWidth(); x++) {
					
					for (int xScale = 0; samePixelAmount ? xScale == 0 : xScale < scale; xScale++) {
						
						int xa = xVal + (x * scale) + (samePixelAmount ? 0 : xScale);
						
						int col = sprite.pixels[x + y * sprite.getWidth()];
						
						if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
							
							continue;
						}
						
						if (col != 0xffff00ff) {
							
							col = ColorHandler.changeBrightness(col, this.brightness.get());
							col = ColorHandler.blend(col, blendColor, ratio);
							
							this.pixels[xa + ya * this.game.getWidth()] = col;
						}
					}
				}
			}
		}
	}
	
	public void renderBrightnessSprite(int brightness, boolean allowOtherBrightnesses, int xVal, int yVal, int scale, boolean fixed, Sprite sprite) {
		
		if (!fixed) {
			
			int cameraX = (int) this.camera.getX(), cameraY = (int) this.camera.getY();
			
			if (
					xVal + sprite.getWidth() < cameraX
					|| yVal + sprite.getHeight() < cameraY
					|| xVal > cameraX + this.camera.getScreenWidth()
					|| yVal > cameraY + this.camera.getScreenHeight()
				) {
				
				return;
			}
			
			xVal -= this.camera.getX();
			yVal -= this.camera.getY();
		}
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			for (int yScale = 0; yScale < scale; yScale++) {
				
				int ya = yVal + (y * scale) + yScale;
				
				for (int x = 0; x < sprite.getWidth(); x++) {
					
					for (int xScale = 0; xScale < scale; xScale++) {
						
						int xa = xVal + (x * scale) + xScale;
						
						int col = sprite.pixels[x + y * sprite.getWidth()];
						
						if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
							
							continue;
						}
						
						if (col != 0xffff00ff) {
							
							col = ColorHandler.changeBrightness(col, brightness);
							
							if (allowOtherBrightnesses) {
								
								col = ColorHandler.changeBrightness(col, this.brightness.get());
							}
							
							this.pixels[xa + ya * this.game.getWidth()] = col;
						}
					}
				}
			}
		}
	}
	
	public void renderPortionSprite(int minX, int maxX, int minY, int maxY, int xVal, int yVal, boolean fixed, Sprite sprite) {
		
		if (!fixed) {
			
			int cameraX = (int) this.camera.getX(), cameraY = (int) this.camera.getY();
			
			if (
					maxX + sprite.getWidth() < cameraX
					|| maxY + sprite.getHeight() < cameraY
					|| minX > cameraX + this.camera.getScreenWidth()
					|| minY > cameraY + this.camera.getScreenHeight()
				) {
				
				return;
			}
			
			xVal -= this.camera.getX();
			yVal -= this.camera.getY();
		}
		
		if (minX == maxX || minY == maxY) {
			
			return;
		}
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			if (y < minY || y > maxY) {
				
				continue;
			}
			
			for (int x = 0; x < sprite.getWidth(); x++) {
				
				if (x < minX || x > maxX) {
					
					continue;
				}
				
				int ya = y + yVal;
				
				int xa = x + xVal;
				
				int col = sprite.pixels[x + y * sprite.getWidth()];
				
				if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
					
					continue;
				}
				
				if (col != 0xffff00ff) {
					
					col = ColorHandler.changeBrightness(col, this.brightness.get());
					
					this.pixels[xa + ya * this.game.getWidth()] = col;
				}
			}
		}
	}
	
	public void renderTile(int xVal, int yVal, Sprite sprite) {
		
		xVal = Tile.TILE_SCALE * xVal * Tile.STANDARD_TILE_SIZE;
		yVal = Tile.TILE_SCALE * yVal * Tile.STANDARD_TILE_SIZE;
		
		int scale = Tile.TILE_SCALE;
		
		int cameraX = (int) this.camera.getX(), cameraY = (int) this.camera.getY();
		
		if (
				xVal + (sprite.getWidth() * scale) < cameraX
				|| yVal + (sprite.getHeight() * scale) < cameraY
				|| xVal > cameraX + this.camera.getScreenWidth()
				|| yVal > cameraY + this.camera.getScreenHeight()
			) {
			
			return;
		}
		
		xVal -= this.camera.getX();
		yVal -= this.camera.getY();
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			for (int yScale = 0; yScale < scale; yScale++) {
				
				int ya = yVal + (y * scale) + yScale;
				
				for (int x = 0; x < sprite.getWidth(); x++) {
					
					for (int xScale = 0; xScale < scale; xScale++) {
						
						int xa = xVal + (x * scale) + xScale;
						
						int col = sprite.pixels[x + y * sprite.getWidth()];
						
						if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
							
							continue;
						}
						
						if (col != 0xffff00ff) {
							
							col = ColorHandler.changeBrightness(col, this.brightness.get());
							
							this.pixels[xa + ya * this.game.getWidth()] = col;
						}
					}
				}
			}
		}
	}
	
	public void fill(int col) {
		
		for (int i = 0; i < this.pixels.length; i++) {
			
			this.pixels[i] = col;
		}
	}
	
	public void setBrightness(int brightness) {
		
		this.brightness.setValue(brightness, true);
	}
	
	public int[] getPixels() {
		
		return this.pixels;
	}
}