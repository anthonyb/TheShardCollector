package com.anthony.foobar;

public class ShutdownHook extends Thread {
	
	@Override
	public void run() {
		
		FooBar.getLogger().destroy();
	}
}