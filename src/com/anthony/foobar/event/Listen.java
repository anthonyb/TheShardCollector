package com.anthony.foobar.event;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Listen {
	
	/**
	 * Define the priority of the event.
	 * <p>
	 * The higher the priority the later the event will be received.<br>
	 * Ex:
	 * <li> Priority 0 executed first.
	 * <li> Priority 1 executed next.
	 * <li> Priority 2 executed next.
	 * </p>
	 */
	int priority() default 0;
}