package com.anthony.foobar.event;

public abstract class Event {
	
	private int id;
	
	/**
	 * Constructor to create a custom Event.
	 * <p>
	 * If creating a custom event, use a large number to prevent event conflict.
	 * @param id
	 */
	public Event(int id) {
		
		this.id = id;
	}
	
	public int getID() {
		
		return this.id;
	}
	
	public abstract String toString();
}