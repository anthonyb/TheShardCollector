package com.anthony.foobar.sound;

import java.util.Random;

public class VarianceSound {
	
	private String primaryPath;
	
	private String[] variancePaths;
	
	private Random random;
	
	public VarianceSound(String primaryPath, String... variancePaths) {
		
		this.primaryPath = primaryPath;
		
		this.variancePaths = variancePaths;
		
		this.random = new Random();
	}
	
	public Sound getPrimarySound() {
		
		return new Sound(this.primaryPath);
	}
	
	public Sound getVarianceSound() {
		
		if (this.variancePaths.length == 0) {
			
			return this.getPrimarySound();
		}
		
		return new Sound(this.variancePaths[this.random.nextInt(this.variancePaths.length)]);
	}
}