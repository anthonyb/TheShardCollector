package com.anthony.foobar.sound;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineEvent.Type;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.anthony.foobar.FooBar;

public class Sound {
	
	private String path;
	
	private Clip clip;
	
	private AudioInputStream ais;
	
	public Sound(String path) {
		
		this.path = path;
		
		try {
			
			ais = AudioSystem.getAudioInputStream(Sound.class.getResource(this.path));
		} catch (IOException e) {
			
			FooBar.getLogger().warn("Could not load sounds " + this.path + " due to an IOException!");
		} catch (UnsupportedAudioFileException e) {
			
			FooBar.getLogger().warn("Could not load sounds " + this.path + " due to an UnsupportedAudioFileException!");
		}
	}
	
	public void play(double volume, boolean loop) {
		
		try {
			
			this.clip = AudioSystem.getClip();
			
			AudioInputStream stream = new AudioInputStream(ais, ais.getFormat(), ais.getFrameLength());
			
			this.clip.open(stream);
			
			if (loop) {
				
				this.clip.loop(Clip.LOOP_CONTINUOUSLY);
			}
			
			FloatControl gainControl = (FloatControl) this.clip.getControl(FloatControl.Type.MASTER_GAIN);
			float gain = (float) (Math.log(volume) / Math.log(10.0) * 20.0);
			gainControl.setValue(gain);
		} catch (LineUnavailableException e) {
			
			FooBar.getLogger().warn("Could not play Sound " + this.path + " due to a LineUnavailableException!");
		} catch (IOException e) {
			
			FooBar.getLogger().warn("Could not play Sound " + this.path + " due to an IOException!");
		}
		
		this.clip.start();
		
		this.clip.addLineListener(
				
			new LineListener() {
				
				@Override
				public void update(LineEvent event) {
					
					if (event.getType() == Type.STOP) {
						
						close();
					}
				}
			}
		);
	}
	
	public void stop() {
		
		this.clip.stop();
	}
	
	private void close() {
		
		this.clip.close();
	}
}