package com.anthony.foobar.threed.camera;

import com.anthony.foobar.threed.math.Matrix;
import com.anthony.foobar.twod.point.Point2D;

public class Camera {
	
	private Point2D point;
	
	private double screenWidth, screenHeight;
	
	public Matrix cameraMatrix;
	
	public Camera(double x, double y, double screenWidth, double screenHeight) {
		
		this.point = new Point2D(x, y);
		
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
	}
	
	public void setX(double x) {
		
		this.point.x = x;
	}
	
	public void setY(double y) {
		
		this.point.y = y;
	}
	
	public double getX() {
		
		return this.point.x;
	}
	
	public double getY() {
		
		return this.point.y;
	}
	
	public Point2D getPoint() {
		
		return this.point;
	}
	
	public double getScreenWidth() {
		
		return this.screenWidth;
	}
	
	public double getHalfScreenWidth() {
		
		return .5 * this.screenWidth;
	}
	
	public double getScreenHeight() {
		
		return this.screenHeight;
	}
	
	public double getHalfScreenHeight() {
		
		return .5 * this.screenHeight;
	}
}